#include "../include/Water.hpp"


Water::Water()
{
      matrix_ = glm::translate(glm::mat4(1.0), glm::vec3(300.0f, -350.0f, 0.0f));
}

Water::~Water()
{
}

void Water::privateInit()
{
    _shader_water.initShaders("/home/daddy/Documents/OpenGL/STE6249/shaders/landscape");
  // Create vertex arrays
    float const X_LENGTH = 8.0f, Z_LENGTH = 40.0f, y = -25.0f;
    for (int i = 0; i < rows; ++i)
    {
      for (int j = 0; j < columns; ++j)
      {
        float x = i*X_LENGTH, z = j*Z_LENGTH;
        //Just one vertex per iteration!
        w_struct.vertices.push_back(glm::vec3{-x, y, -z});
        w_struct.textureCoordinates.push_back(glm::vec2{float(i)/rows, float(j)/columns});
      }
    }

    //Sets up the indices we need for the trianglestrip
    for (int i = 1; i < rows; ++i)
    {
      for (int j = 0; j < columns; ++j)
      {
        const auto& val1 = (i*rows) + j;
        const auto& val2 = ((i-1)*rows) + j;

        w_struct.indices.push_back(val1);
        w_struct.indices.push_back(val2);
      }
      w_struct.indices.push_back(UINT_MAX);
    }

    //Loading Images
    water_id_ = SOIL_load_OGL_cubemap(
              (skybox_path_ + "east.bmp").c_str(),
              (skybox_path_ + "west.bmp").c_str(),
              (skybox_path_ + "up.bmp").c_str(),
              (skybox_path_ + "down.bmp").c_str(),
              (skybox_path_ + "south.bmp").c_str(),
              (skybox_path_ + "north.bmp").c_str(),
        SOIL_LOAD_RGB,
        SOIL_CREATE_NEW_ID,
        SOIL_FLAG_MIPMAPS);

    if (0 == water_id_) {
        std::cout << "SOIL: water FAILED >> error: '%s'\n"
                  << SOIL_last_result()
                  << std::endl;
    }
    //End of Loading Images
    _shader_water.initShaders("/home/daddy/Documents/OpenGL/STE6249/shaders/water");
    _shader_water.enable();

    GLuint program = _shader_water.getProg();
    GLint location = glGetUniformLocation(program, "waterMap");
    glUniform1i(location , 0);

    _shader_water.disable();
}

void Water::privateRender()
{
    _shader_water.enable();

    glEnable(GL_TEXTURE_CUBE_MAP);
    glBindTexture(GL_TEXTURE_CUBE_MAP, water_id_);

    glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP);
    glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP);
    glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP);
    glEnable(GL_TEXTURE_GEN_S);
    glEnable(GL_TEXTURE_GEN_T);
    glEnable(GL_TEXTURE_GEN_R);

    GLint waveLocation = glGetUniformLocation(_shader_water.getProg(), "wave");
    glUniform1f(waveLocation, waterLevel += 30);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    //Water
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &w_struct.vertices[0]);
    glEnable(GL_PRIMITIVE_RESTART);
    glPrimitiveRestartIndex(UINT_MAX);
    glDrawElements(GL_TRIANGLE_STRIP, w_struct.indices.size(),
                   GL_UNSIGNED_INT, &w_struct.indices[0]);
    glDisable(GL_PRIMITIVE_RESTART);
    glDisableClientState(GL_VERTEX_ARRAY);

    glDisable(GL_TEXTURE_2D);
    glDisable(GL_TEXTURE_CUBE_MAP);
    glDisable(GL_TEXTURE_GEN_S);
    glDisable(GL_TEXTURE_GEN_T);
    glDisable(GL_TEXTURE_GEN_R);
    glDisable(GL_BLEND);

    _shader_water.disable();
}

void Water::privateUpdate()
{
}
