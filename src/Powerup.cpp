#include "include/Powerup.hpp"

Powerup::Powerup()
{
}

Powerup::~Powerup()
{
}

void Powerup::privateInit()
{
    shader_powerup.initShaders("/home/daddy/Documents/OpenGL/STE6249/shaders/phong");
}

void Powerup::privateRender()
{
    shader_powerup.enable();
    glColor3f(2.26f, 2.55f, 0.13f);
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_DIFFUSE);
    GLfloat ambient[] = { 0.0, 0.0, 0.0, 1.0 };
    GLfloat spec[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
    glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
    glMaterialfv(GL_FRONT, GL_SPECULAR, spec);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);

    glutSolidSphere(3, 20, 10);
    glDisable(GL_COLOR_MATERIAL);
    glClearColor(0.0, 0.0, 0.0, 1.0);
    shader_powerup.disable();
}

void Powerup::privateUpdate()
{
    matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, 1.0f));
}

bool Powerup::markedForDeletion()
{
    return _markDelete;
}
