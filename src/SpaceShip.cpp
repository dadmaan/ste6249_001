#include "../include/SpaceShip.hpp"

SpaceShip::SpaceShip(const DataStructure&       data)
{
    data_spaceship = data;
}

SpaceShip::~SpaceShip()
{
}

void SpaceShip::privateInit()
{
    shader_spaceship.initShaders("/home/daddy/Documents/OpenGL/STE6249/shaders/spaceship");

    tex_ = loadTexture("/home/daddy/Documents/OpenGL/STE6249/materials/models/spaceship/viper/viper.png");

    matrix_ = glm::translate(matrix_, glm::vec3(0.0f, -10.0f, -30.0f));
    matrix_ = glm::rotate(matrix_, -PI/2, glm::vec3(0, 1, 0));
    matrix_ = glm::scale(matrix_, glm::vec3(2.0f, 2.0f, 2.0f));
}

void SpaceShip::privateRender()
{
    shader_spaceship.enable();

    glDisable(GL_COLOR_MATERIAL);

    glClearColor(0.0, 0.0, 0.0, 0.0);

    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex_);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glEnable(GL_PRIMITIVE_RESTART);

    glVertexPointer(3, GL_FLOAT, 0, &data_spaceship.vertices[0]);
    glTexCoordPointer(2, GL_FLOAT, 0, &data_spaceship.textureCoordinates[0]);

    glDrawElements(GL_TRIANGLES, data_spaceship.indices.size(),
                   GL_UNSIGNED_INT, &data_spaceship.indices[0]);

    glDisable(GL_PRIMITIVE_RESTART);

    glClearColor(0.0, 0.0, 0.0, 0.0);

    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_2D);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);

    shader_spaceship.disable();
}
void SpaceShip::privateUpdate()
{
    matrix_ = glm::translate(matrix_, glm::vec3(-0.15f, 0.0f, 0.0f));
}

void SpaceShip::changeWeapon(const int &weapon)
{
    weapon_ = weapon;
}

int SpaceShip::getWeapon()
{
    return weapon_;
}

int SpaceShip::getLife()
{
    return life_;
}

void SpaceShip::moveRight()
{
    matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, -0.5f));
}
void SpaceShip::moveLeft()
{
    matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, 0.5f));
}
void SpaceShip::moveForward()
{
    matrix_ = glm::translate(matrix_, glm::vec3(-0.25f , 0.0f, 0.0f));
}

void SpaceShip::moveBackward()
{
    matrix_ = glm::translate(matrix_, glm::vec3(0.15f, 0.0f, 0.0f));
}
