#include <math.h>
#include "../include/Weapons.hpp"


Weapons::Weapons(const glm::mat4& startMatrix,
                 Shooter s,
                 float speed)
{
    setMatrix(startMatrix);
    _trajectory = s;
    speed_ = speed;

    //Since we rotated the spaceship,
    //it's neccessary to rotate the direction of weapon too.
    if (_trajectory == Shooter::SPACESHIP)
        matrix_ = glm::rotate(matrix_, PI/2, glm::vec3(0, 1, 0));
        matrix_ = glm::scale(matrix_, glm::vec3(2.0f, 2.0f, 2.0f));
}

Weapons::~Weapons()
{
}

void Weapons::privateInit()
{
    shader_weapon.initShaders("/home/daddy/Documents/OpenGL/STE6249/shaders/phong");
}

void Weapons::privateRender()
{
    shader_weapon.enable();
    if (_trajectory == Shooter::SPACESHIP)
    {
        glColor3f(1.74f, 0.67f, 0.0f);
        glutSolidSphere(0.25, 20, 10);
        glClearColor(0.0, 0.0, 0.0, 1.0);
    }
    else if (_trajectory == Shooter::ENEMY)
    {
        glColor3f(1.0f, 0.0f, 0.0f);
        glutSolidSphere(0.25, 20, 10);
        glClearColor(0.0, 0.0, 0.0, 1.0);
    }
    else if (_trajectory == Shooter::UFO)
    {
        glColor3f(0.0f, 0.0f, 0.0f);
        glutSolidSphere(0.35, 20, 10);
        glClearColor(0.0, 0.0, 0.0, 1.0);
    }
    shader_weapon.disable();
}

void Weapons::privateUpdate()
{
    if (_trajectory == Shooter::SPACESHIP)
    {
        matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, -speed_));
    }
    else if (_trajectory == Shooter::ENEMY)
    {
        matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, speed_ ));
    }
    else if (_trajectory == Shooter::UFO)
    {
        matrix_ = glm::translate(matrix_, glm::vec3(0.0f, -speed_/10, 0.0f ));
    }
}

