#pragma once
#include "include/Particles.hpp"

ParticleGenerator::ParticleGenerator(GLuint& texture,
                                     ParticleGenerator::Type genType,
                                     std::shared_ptr<Weapons> weapons)
{
    weapons_    = weapons;
    tex_        = texture;
    type        = genType;

    if (type == ParticleGenerator::Type::BULLET){
        auto w_matrix = weapons_->getMatrix();
        matrix_ = glm::translate(matrix_, glm::vec3(w_matrix[3].x, w_matrix[3].y, w_matrix[3].z + -20.0f));
    }

    this->_init();
}

ParticleGenerator::~ParticleGenerator()
{

}

void ParticleGenerator::_init()
{
    shader_particle.initShaders("/home/daddy/Documents/OpenGL/STE6249/shaders/particle");
    if (type == ParticleGenerator::Type::EXPLOSION)
    {
        for (int loop = 0; loop<MAX_PARTICLES; loop++)                      // Initials All The Textures
        {
            particle[loop].active = true;                                   // Make All The Particles Active
            particle[loop].life = 1.0f;                                     // Give All The Particles Full Life
            particle[loop].fade = float(rand() % 100) / 1000.0f + 0.003f;	// Random Fade Speed
            particle[loop].r = colors[loop*(12 / MAX_PARTICLES)][0];        // Select Red Rainbow Color
            particle[loop].g = colors[loop*(12 / MAX_PARTICLES)][1];        // Select Red Rainbow Color
            particle[loop].b = colors[loop*(12 / MAX_PARTICLES)][2];        // Select Red Rainbow Color
            particle[loop].xi = float((rand() % 50) - 26.0f)*10.0f;         // Random Speed On X Axis
            particle[loop].yi = float((rand() % 50) - 25.0f)*10.0f;         // Random Speed On Y Axis
            particle[loop].zi = float((rand() % 50) - 25.0f)*10.0f;         // Random Speed On Z Axis
            particle[loop].xg = 0.0f;                                       // Set Horizontal Pull To Zero
            particle[loop].yg = -0.8f;                                      // Set Vertical Pull Downward
            particle[loop].zg = 0.0f;                                       // Set Pull On Z Axis To Zero
            particle[loop].x = matrix_[3].x;
            particle[loop].y = matrix_[3].y;
            particle[loop].z = matrix_[3].z;
        }
    } else if (type == ParticleGenerator::Type::BULLET)
    {
        for (int loop = 0; loop<MAX_PARTICLES_BULLET; loop++)               // Initials All The Textures
        {
            particle[loop].active   = true;                                     // Make All The Particles Active
            particle[loop].life     = 1.0f;                                     // Give All The Particles Full Life
            particle[loop].fade     = float(rand() % 100) / 1000.0f + 0.003f;	// Random Fade Speed
            particle[loop].r        = colors[loop*(12 / MAX_PARTICLES)][0];    // Select Red Rainbow Color
            particle[loop].g        = colors[loop*(12 / MAX_PARTICLES)][1];    // Select Red Rainbow Color
            particle[loop].b        = colors[loop*(12 / MAX_PARTICLES)][2];    // Select Red Rainbow Color
            particle[loop].xi       = float((rand() % 50) - 26.0f)*10.0f;       // Random Speed On X Axis
            particle[loop].yi       = float((rand() % 50) - 25.0f)*10.0f;       // Random Speed On Y Axis
            particle[loop].zi       = float((rand() % 50) - 25.0f)*10.0f;       // Random Speed On Z Axis
            particle[loop].xg       = 0.0f;                                     // Set Horizontal Pull To Zero
            particle[loop].yg       = -0.8f;                                    // Set Vertical Pull Downward
            particle[loop].zg       = 0.0f;                                     // Set Pull On Z Axis To Zero
            particle[loop].x        = matrix_[3].x;
            particle[loop].y        = matrix_[3].y;
            particle[loop].z        = matrix_[3].z;
        }
    }

}

void ParticleGenerator::privateInit()
{
}

void ParticleGenerator::privateRender()
{
    if (type == ParticleGenerator::Type::EXPLOSION)
    {
        explosion();
    }
    else if (type == ParticleGenerator::Type::BULLET)
    {
        bullet();
    }
}

void ParticleGenerator::bullet()
{
    shader_particle.enable();

    GLuint colour = glGetUniformLocation(shader_particle.getProg(), "colour");
    glUniform1i(colour, 0);
    glShadeModel(GL_SMOOTH);                                            // Enable Smooth Shading
    glClearDepth(1.0);                                                  // Depth Buffer Setup
    glDisable(GL_DEPTH_TEST);                                           // Disable Depth Testing
    glEnable(GL_BLEND);                                                 // Enable Blending
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);									// Type Of Blending To Perform
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);                  // Really Nice Perspective Calculations
    glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);                            // Really Nice Point Smoothing
    glEnable(GL_TEXTURE_2D);                                            // Enable Texture Mapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, tex_);
    fadeEffect = glGetUniformLocation(shader_particle.getProg(), "fade");

    auto matrix = weapons_->getMatrix();
    for (int loop = 0; loop < MAX_PARTICLES_BULLET; loop++)		// Loop Through All The Particles
    {
        if (particle[loop].active)                              // If The Particle Is Active
        {
            float x = particle[loop].x;                         // Grab Our Particle X Position
            float y = particle[loop].y;                         // Grab Our Particle Y Position
            float z = particle[loop].z;                         // Particle Z Pos + Zoom

            // Draw The Particle Using Our RGB Values, Fade The Particle Based On It's Life
            glColor4f(particle[loop].r, particle[loop].g, particle[loop].b, particle[loop].life);

            glBegin(GL_TRIANGLE_STRIP);                                 // Build Quad From A Triangle Strip
            glTexCoord2d(1, 1); glVertex3f(x + 0.25f, y + 0.25f, z);      // Top Right
            glTexCoord2d(0, 1); glVertex3f(x - 0.25f, y + 0.25f, z);      // Top Left
            glTexCoord2d(1, 0); glVertex3f(x + 0.25f, y - 0.25f, z);      // Bottom Right
            glTexCoord2d(0, 0); glVertex3f(x - 0.25f, y - 0.25f, z);      // Bottom Left
            glEnd();                                                    // Done Building Triangle Strip

            particle[loop].x += particle[loop].xi / (bullet_slowdown * 10);  // Move On The X Axis By X Speed
            particle[loop].y += particle[loop].yi / (bullet_slowdown * 10);  // Move On The Y Axis By Y Speed
            particle[loop].z += particle[loop].zi / (bullet_slowdown * 10);  // Move On The Z Axis By Z Speed

            particle[loop].xi += particle[loop].xg + matrix[3].x;		// Take Pull On X Axis Into Account
            particle[loop].yi += particle[loop].yg + matrix[3].y;   	// Take Pull On Y Axis Into Account
            particle[loop].zi += particle[loop].zg + matrix[3].z;       // Take Pull On Z Axis Into Account

            glUniform1f(fadeEffect,1.0f);
        }
    }
    glEnable(GL_DEPTH_TEST);                                    // Disable Depth Testing
    glDisable(GL_BLEND);                                        // Enable Blending
    glDisable(GL_TEXTURE_2D);                                   // Enable Texture Mapping

    shader_particle.disable();
}

void ParticleGenerator::explosion()
{
    shader_particle.enable();

    GLuint colour = glGetUniformLocation(shader_particle.getProg(), "colour");
    glUniform1i(colour, 0);
    glShadeModel(GL_SMOOTH);														// Enable Smooth Shading
    glClearDepth(1.0f);																	// Depth Buffer Setup
    glDisable(GL_DEPTH_TEST);														// Disable Depth Testing
    glEnable(GL_BLEND);																	// Enable Blending
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);									// Type Of Blending To Perform
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
    glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);						// Really Nice Point Smoothing
    glEnable(GL_TEXTURE_2D);														// Enable Texture Mapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, tex_);
    fadeEffect = glGetUniformLocation(shader_particle.getProg(), "fade");

    for (int loop = 0; loop < MAX_PARTICLES; loop++)        // Loop Through All The Particles
    {
        if (particle[loop].active)                          // If The Particle Is Active
        {
            float x = particle[loop].x;                     // Grab Our Particle X Position
            float y = particle[loop].y;                     // Grab Our Particle Y Position
            float z = particle[loop].z;                     // Particle Z Pos + Zoom

            // Draw The Particle Using Our RGB Values, Fade The Particle Based On It's Life
            glColor4f(particle[loop].r, particle[loop].g, particle[loop].b, particle[loop].life);

            glBegin(GL_TRIANGLE_STRIP);                                 // Build Quad From A Triangle Strip
            glTexCoord2d(1, 1); glVertex3f(x + 0.5f, y + 0.5f, z);      // Top Right
            glTexCoord2d(0, 1); glVertex3f(x - 0.5f, y + 0.5f, z);      // Top Left
            glTexCoord2d(1, 0); glVertex3f(x + 0.5f, y - 0.5f, z);      // Bottom Right
            glTexCoord2d(0, 0); glVertex3f(x - 0.5f, y - 0.5f, z);      // Bottom Left
            glEnd();                                                    // Done Building Triangle Strip

            particle[loop].x += particle[loop].xi / (explosion_slowdown * 1000);  // Move On The X Axis By X Speed
            particle[loop].y += particle[loop].yi / (explosion_slowdown * 1000);  // Move On The Y Axis By Y Speed
            particle[loop].z += particle[loop].zi / (explosion_slowdown * 1000);  // Move On The Z Axis By Z Speed

            particle[loop].xi += particle[loop].xg;                     // Take Pull On X Axis Into Account
            particle[loop].yi += particle[loop].yg;                     // Take Pull On Y Axis Into Account
            particle[loop].zi += particle[loop].zg;                     // Take Pull On Z Axis Into Account

            particle[loop].life -= particle[loop].fade;                 // Reduce Particles Life By 'Fade'
            glUniform1f(fadeEffect, particle[loop].life);

            if (particle[loop].life < 0.0f)
            {
                particle[loop].life = 0.0f;
                particle[loop].fade = float(rand() % 100) / 1000.0f + 0.003f;
                particle[loop].xi = x_speed + float((rand() % 60) - 32.0f);
                particle[loop].yi = y_speed + float((rand() % 60) - 30.0f);
                particle[loop].zi = float((rand() % 60) - 30.0f);
                faded = true;
            }
        }

    }
    glEnable(GL_DEPTH_TEST);							// Disable Depth Testing
    glDisable(GL_BLEND);                                // Disable Blending
    glDisable(GL_TEXTURE_2D);							// Disable Texture Mapping

    shader_particle.disable();
}

void ParticleGenerator::privateUpdate()
{

}
