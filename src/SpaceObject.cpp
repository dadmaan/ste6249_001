#include "../include/SpaceObject.hpp"

SpaceObject::SpaceObject(const DataStructure& object,
                         const GLuint& texture,
                         float pos_x,
                         float pos_z,
                         const SpaceObject::Trajectory& t)
{
    object_spaceobject = object;
    tex_ = texture;
    trajectory_ = t;
    position_x = pos_x;
    position_z = pos_z;
    matrix_ = glm::translate(glm::mat4(1.0), glm::vec3(position_x, 15.0f, -position_z));
    this->privateInit();
}

SpaceObject::~SpaceObject()
{
}

void SpaceObject::privateInit()
{
    shader_spaceobject.initShaders("/home/daddy/Documents/OpenGL/STE6249/shaders/ufo");
    float size = 2.5f;
    matrix_ = glm::scale(matrix_, glm::vec3(size, size, size));
}

void SpaceObject::privateRender()
{
    shader_spaceobject.enable();

    glDisable(GL_COLOR_MATERIAL);

    glClearColor(0.0, 0.0, 0.0, 0.0);

    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex_);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glEnable(GL_PRIMITIVE_RESTART);

    glVertexPointer(3, GL_FLOAT, 0, &object_spaceobject.vertices[0]);
    glTexCoordPointer(2, GL_FLOAT, 0, &object_spaceobject.textureCoordinates[0]);

    glDrawElements(GL_TRIANGLES, object_spaceobject.indices.size(),
                   GL_UNSIGNED_INT, &object_spaceobject.indices[0]);

    glDisable(GL_PRIMITIVE_RESTART);

    glClearColor(0.0, 0.0, 0.0, 0.0);

    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_2D);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);

    shader_spaceobject.disable();
}
void SpaceObject::privateUpdate()
{
    if (trajectory_ == Trajectory::RIGHT)
    {
        if (matrix_[3].x <= 20)
            matrix_ = glm::translate(matrix_, glm::vec3(0.05f, 0.0f, -0.02f));

        else{
            trajectory_ = Trajectory::LEFT;
        }
    }
    else if (trajectory_ == Trajectory::LEFT)
    {
        if (matrix_[3].x >= -20)
            matrix_ = glm::translate(matrix_, glm::vec3(-0.05f, 0.0f, 0.02f));

        else{
            trajectory_ = Trajectory::RIGHT;
        }
    }
}

