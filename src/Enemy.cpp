#include "../include/Enemy.hpp"

Enemy::Enemy(const SceneObject::DataStructure &object,
             const GLuint& texture,
             const glm::vec3 &translate,
             const Enemy::Trajectory &t)
{
    object_enemy = object;
    tex_ = texture;
    matrix_ = glm::translate(glm::mat4(1.0), translate);
    trajectory_ = t;
    this->privateInit();
}

Enemy::~Enemy()
{
}

void Enemy::privateInit()
{
    shader_enemy.initShaders("/home/daddy/Documents/OpenGL/STE6249/shaders/spaceship");
    float size = 4.5f;
    matrix_ = glm::scale(matrix_, glm::vec3(size, size, size));
}

void Enemy::privateRender()
{
    shader_enemy.enable();

    glDisable(GL_COLOR_MATERIAL);

    glClearColor(0.0, 0.0, 0.0, 0.0);

    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex_);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glEnable(GL_PRIMITIVE_RESTART);

    glVertexPointer(3, GL_FLOAT, 0, &object_enemy.vertices[0]);
    glTexCoordPointer(2, GL_FLOAT, 0, &object_enemy.textureCoordinates[0]);

    glDrawElements(GL_TRIANGLES, object_enemy.indices.size(),
                   GL_UNSIGNED_INT, &object_enemy.indices[0]);

    glDisable(GL_PRIMITIVE_RESTART);

    glClearColor(0.0, 0.0, 0.0, 0.0);

    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_2D);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);

    shader_enemy.disable();
}
void Enemy::privateUpdate()
{
    if (trajectory_ == Trajectory::FORWARD)
    {
        matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, 0.4f));
    }

    else if (trajectory_ == Trajectory::RIGHT)
    {
        if (matrix_[3].x <= 80)
        {
            matrix_ = glm::translate(matrix_, glm::vec3(0.25f, 0.0f, 0.25f));
        }
        else {
            trajectory_ = Trajectory::LEFT;
        }
    }
    else if (trajectory_ == Trajectory::LEFT)
    {
        if (matrix_[3].x >= -80)
        {
            matrix_ = glm::translate(matrix_, glm::vec3(-0.25f, 0.0f, 0.25f));
        }
        else {
            trajectory_ = Trajectory::RIGHT;
        }
    }
}

