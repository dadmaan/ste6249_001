﻿#include "../include/Skybox.hpp"

Skybox::Skybox()
{
}

Skybox::~Skybox()
{
}

void Skybox::privateInit()
{

  shader_skybox.initShaders("/home/daddy/Documents/OpenGL/STE6249/shaders/skybox");
  //Loading Images
  skybox_id_ = SOIL_load_OGL_cubemap(
            (skybox_path_ + "orbital/east.tga").c_str(),
            (skybox_path_ + "orbital/west.tga").c_str(),
            (skybox_path_ + "orbital/up.tga").c_str(),
            (skybox_path_ + "orbital/down.tga").c_str(),
            (skybox_path_ + "orbital/south.tga").c_str(),
            (skybox_path_ + "orbital/north.tga").c_str(),
      SOIL_LOAD_RGB,
      SOIL_CREATE_NEW_ID,
      SOIL_FLAG_MIPMAPS);

  if (0 == skybox_id_) {
      std::cout << "SOIL: Loading SkyBox Texture FAILED >> error: '%s'\n"
                << SOIL_last_result()
                << std::endl;
  }
  else {
      std::cout << "SOIL: SkyBox Texture Successfully Loaded!"
                << std::endl;
  }
  //End of Loading Images
  shader_skybox.enable();

  GLuint program = shader_skybox.getProg();
  GLint location = glGetUniformLocation(program, "skyBoxMap");
  glUniform1i(location ,0);

  shader_skybox.disable();
}

void Skybox::privateRender()
{
  shader_skybox.enable();

  glFrontFace(GL_CW);
  glDisable(GL_DEPTH_TEST);

  glEnable(GL_TEXTURE_CUBE_MAP);
  glBindTexture(GL_TEXTURE_CUBE_MAP, skybox_id_);

  glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP);
  glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP);
  glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP);
  glEnable(GL_TEXTURE_GEN_S);
  glEnable(GL_TEXTURE_GEN_T);
  glEnable(GL_TEXTURE_GEN_R);

  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

  // Draw Cube
  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(3, GL_FLOAT, 0, &vertexArray_[0]);
  glDrawElements(GL_TRIANGLES, static_cast<int>(indexArray_.size()),
                 GL_UNSIGNED_INT, &indexArray_[0]);
  glDisableClientState(GL_VERTEX_ARRAY);

  glDisable(GL_TEXTURE_CUBE_MAP);
  glDisable(GL_TEXTURE_GEN_S);
  glDisable(GL_TEXTURE_GEN_T);
  glDisable(GL_TEXTURE_GEN_R);
  glEnable(GL_DEPTH_TEST);
  glFrontFace(GL_CW);

  shader_skybox.disable();
}

void Skybox::privateUpdate()
{
}

GLuint Skybox::getSkybox()
{
  return skybox_id_;
}

void Skybox::setPos(const glm::vec3 &vec)
{
    matrix_ = glm::mat4(1.0);
    matrix_ = glm::translate(matrix_, vec);
}
