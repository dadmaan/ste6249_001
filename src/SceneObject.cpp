#include "../include/SceneObject.hpp"
#include "../include/OBJ_Loader.h"

//#include <windows.h>
#include <GL/glew.h>
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"

// NB! Check matrix mult and scoped_ptr

SceneObject::SceneObject()
{
  //setIdentity(matrix_);
  matrix_ = glm::mat4(1.0);
}

SceneObject::~SceneObject()
{
}

void SceneObject::render()
{
  glPushMatrix();
    //this->matrix_.multMatrix();
    glMultMatrixf(glm::value_ptr(matrix_));
    this->privateRender();
    for(std::vector<std::shared_ptr<SceneObject> >::iterator it = children_.begin();
        it != children_.end(); it++)
        (*it)->render();
  glPopMatrix();
}

void SceneObject::update(double fps)
{
  this->fps_ = fps;
  this->dt   = 1/fps;
  this->privateUpdate();
  for(std::vector<std::shared_ptr<SceneObject> >::iterator it = children_.begin();
      it != children_.end(); it++)
      (*it)->update(fps);
}

void SceneObject::init()
{
  this->privateInit();
  for(std::vector<std::shared_ptr<SceneObject> >::iterator it = children_.begin();
      it != children_.end(); it++)
      (*it)->init();
}

void SceneObject::addSubObject(std::shared_ptr<SceneObject> newchild)
{
  children_.push_back(newchild);
}

void SceneObject::removeSubObject(const std::shared_ptr<SceneObject> child)
{
  for(std::vector<std::shared_ptr<SceneObject> >::iterator it = children_.begin();
      it != children_.end(); it++)
    if(*it == child)
    {
      children_.erase(it);
      break;
    }
}

SceneObject::DataStructure SceneObject::loadObject(const char *file)
{
    DataStructure data;
    objl::Loader objectLoader;
    bool success = objectLoader.LoadFile(models_path + file);

    if (success) {
      for (int i = 0; i < objectLoader.LoadedMeshes.size(); i++) {
        objl::Mesh curMesh = objectLoader.LoadedMeshes[i];
        for (int j = 0; j < curMesh.Vertices.size(); j++) {
          data.vertices.push_back(glm::vec3(curMesh.Vertices[j].Position.X, curMesh.Vertices[j].Position.Y, curMesh.Vertices[j].Position.Z));
          data.normals.push_back(glm::vec3(curMesh.Vertices[j].Normal.X, curMesh.Vertices[j].Normal.Y, curMesh.Vertices[j].Normal.Z));
          data.textureCoordinates.push_back(glm::vec2(curMesh.Vertices[j].TextureCoordinate.X, curMesh.Vertices[j].TextureCoordinate.Y));
        }

        for (int j = 0; j < curMesh.Indices.size(); j += 3) {
          data.indices.push_back(curMesh.Indices[j]);
          data.indices.push_back(curMesh.Indices[j + 1]);
          data.indices.push_back(curMesh.Indices[j + 2]);
        }
      }
    }
    else {
      std::cout << "Loading " << file << "d Failed!" << std::endl;
    }
    return data;
}

unsigned int SceneObject::loadTexture(const char *file)
{
    unsigned int tex_ = SOIL_load_OGL_texture(
                file,
                SOIL_LOAD_AUTO,
                SOIL_CREATE_NEW_ID,
                SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
    /* check for an error during the load process */
    if (0 == tex_) {
      std::cout << "SOIL Failed: "
                << file
                << ">> error: '%s'\n"
                << SOIL_last_result()
                << std::endl;
    }
    else if(!tex_success)  {
        tex_success = true;
        std::cout << "SOIL Success: "
                  << file
                  << std::endl;
    }
    return tex_;
}

bool SceneObject::detectCollision(glm::vec3 object_a,
                                  float ra,
                                  glm::vec3 object_b,
                                  float rb)
{
    float a_radius = ra;
    float b_radius = rb;
    float dist = glm::length(object_a - object_b);

    if (dist < a_radius + b_radius)
    {
        std::cout << "Ouch!" << std::endl;
        return true;
    }
    else {
        return false;
    }
}

