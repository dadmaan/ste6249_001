#include "../include/GameManager.hpp"
#include "Audio.cpp"

GameManager::GameManager()
{
}

GameManager::~GameManager()
{
}

void GameManager::privateInit()
{
    // Set default OpenGL states
    glEnable(GL_CULL_FACE);

    //Loading Textures
    enemy_tex               = loadTexture("/home/daddy/Documents/OpenGL/STE6249/materials/models/enemy/droid/droid.png");
    spaceobject_tex         = loadTexture("/home/daddy/Documents/OpenGL/STE6249/materials/models/spaceobject/ufo/ufo.png");
    particle_explosion_tex  = loadTexture("/home/daddy/Documents/OpenGL/STE6249/materials/particle/explosion_particle.bmp");

    //Light position
    float lightPos[] = { 1.0f, 1.0f, 1.0f, 0.0f };
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

    // Adding the camera to the scene
    cam_.reset(new Camera());
    this->addSubObject(cam_);

    skybox_.reset(new Skybox());
    this->addSubObject(skybox_);

    //Battle-Field
    bf_.reset(new BattleField());
    this->addSubObject(bf_);

    //Water
    water_.reset(new Water());
    this->addSubObject(water_);

    //Spaceship
    spaceship_obj_data = loadObject("spaceship/viper/viper.obj");
    spaceship_.reset(new SpaceShip(spaceship_obj_data));
    this->addSubObject(spaceship_);

    //Enemies
    enemy_obj_data = loadObject("enemy/droid/droid.obj");
    enemy2_obj_data = loadObject("enemy/arwing/arwing.obj");

    //UFO
    spaceobject_obj_data = loadObject("spaceobject/ufo/ufo.obj");

    clock_.start();
  glDisable(GL_CULL_FACE);
}

void GameManager::privateRender()
{
  // Nothing to render
}

void GameManager::privateUpdate()
{
  // Instead of adding alle objectopen gl errorinvalid values in the scene as subobject to the camera they are added as subobjects
  // to the game manager. Therefore, we set the game manager matrix equal to the camera matrix. 
    this->matrix_ = cam_->getMatrix();
    skybox_->setPos(glm::vec3(-matrix_[3][0], -matrix_[3][1], -matrix_[3][2]));

    if(gameStart == true){
        this->EnemyFire();
        this->generateEnemy();
        this->generateUFO();
        this->ufoFire();

        //Detect Collision
        this->enemyShootPlayer();
        this->playerShootEnemy();
        this->playerHitEnemy();
        this->playerPowerup();

        //Cleanup Enviroment
        this->shootingCleanup();
        this->enemyCleanup();
        this->ufoCleanup();
        this->powerupCleanup();

        this->removeLastGeneratedParticles();
        if(spaceship_->isPoweredUp)
        {
            this->removePowerup();
        }
    }
}

void GameManager::generateEnemy()
{
    //Adjust enemy's position base on the camera z position
    const float z_pos = -(this->cam_->getMatrix()[3].z + 450.0f);
    float y_pos = -10.0f;

    if (!generateEnemy_flag)
    {
        generateEnemy_flag = true;
        lastGeneratedEnemy = TimeMachine::now();
        Enemy::Trajectory trajectoryEnemy;

        if (wave1){
            trajectoryEnemy = Enemy::Trajectory::FORWARD;

            for (int i = 0; i < random(1, 2); i++)
            {
                enemy_.reset(new Enemy(enemy_obj_data,
                                       enemy_tex,
                                       glm::vec3{float(randomExcludeZero(-20, 20) + (5 * (i + 1))), y_pos, z_pos},
                                       trajectoryEnemy));
                enemy_->speed_ = 50.0f;
                this->addSubObject(enemy_);
                enemies_array.push_back(enemy_);
            }
        }
        else if (wave2) {
            enemyGenerationTimeSpan = 1800;

            int randomTrajectory = random(0, 1);
            if (randomTrajectory == 0) {
                trajectoryEnemy = Enemy::Trajectory::RIGHT;
            }
            else if (randomTrajectory == 1) {
                trajectoryEnemy = Enemy::Trajectory::LEFT;
            }

            for (int i = 0; i < random(1, 3); i++)
            {
                enemy_.reset(new Enemy(enemy2_obj_data,
                                       enemy_tex,
                                       glm::vec3{float(randomExcludeZero(-20, 20) + (5 * (i + 1))), y_pos, z_pos},
                                       trajectoryEnemy));
                enemy_->speed_ = 50.0f;
                this->addSubObject(enemy_);
                enemies_array.push_back(enemy_);
            }
        }
        else if (wave3 or wave4) {
            if(wave3) enemyGenerationTimeSpan = 1000;
            if(wave4) enemyGenerationTimeSpan = 500;

            trajectoryEnemy = Enemy::Trajectory::FORWARD;

            for (int i = 0; i < random(1, 2); i++)
            {
                enemy_.reset(new Enemy(enemy_obj_data,
                                       enemy_tex,
                                       glm::vec3{float(randomExcludeZero(-20, 20) + (5 * (i + 1))), y_pos, z_pos},
                                       trajectoryEnemy));
                enemy_->speed_ = 250.0f;
                this->addSubObject(enemy_);
                enemies_array.push_back(enemy_);
            }
            int randomTrajectory = random(0, 1);
            if (randomTrajectory == 0) {
                trajectoryEnemy = Enemy::Trajectory::RIGHT;
            }
            else if (randomTrajectory == 1) {
                trajectoryEnemy = Enemy::Trajectory::LEFT;
            }

            for (int i = 0; i < random(1, 3); i++)
            {
                enemy_.reset(new Enemy(enemy2_obj_data,
                                       enemy_tex,
                                       glm::vec3{float(randomExcludeZero(-20, 20) + (5 * (i + 1))), y_pos, z_pos},
                                       trajectoryEnemy));
                enemy_->speed_ = 150.0f;
                this->addSubObject(enemy_);
                enemies_array.push_back(enemy_);
            }
        }
    }
    else if (duration_cast<milliseconds>(TimeMachine::now() - lastGeneratedEnemy).count() > enemyGenerationTimeSpan)
    {
        generateEnemy_flag = false;
    }
}

void GameManager::generateUFO()
{
    SpaceObject::Trajectory spaceobject_trajectory;

    if(!generateUFO_flag){
        generateUFO_flag = true;
        lastGeneratedUFO = TimeMachine::now();

        for (int i = 1; i < 3; ++i) {
            auto z = cam_->getMatrix()[3].z + 50.0f;
            spaceobject_trajectory = SpaceObject::Trajectory::RIGHT;
            spaceobject_.reset(new SpaceObject(spaceobject_obj_data,
                                               spaceobject_tex,
                                               10.0f,
                                               float(i*z + 10.0f),
                                               spaceobject_trajectory));
            this->addSubObject(spaceobject_);
            ufos_array.push_back(spaceobject_);

            spaceobject_trajectory = SpaceObject::Trajectory::LEFT;
            spaceobject_.reset(new SpaceObject(spaceobject_obj_data,
                                               spaceobject_tex,
                                               -10.0f,
                                               float(i*z + 20.0f),
                                               spaceobject_trajectory));
            this->addSubObject(spaceobject_);
            ufos_array.push_back(spaceobject_);
        }
    }
    else if (duration_cast<milliseconds>(TimeMachine::now() - lastGeneratedUFO).count() > ufoGenerationTimeSpan)
    {
        generateUFO_flag = false;
    }
}

void GameManager::EnemyFire()
{
    if (!enemyFire_flag)
    {
        //Adjust the difficulty for each waves
        if(wave2) nextFireFromEnemy = 250;
        if(wave3) nextFireFromEnemy = 200;
        if(wave4) nextFireFromEnemy = 150;

        enemyFire_flag = true;
        lastEnemyFire = TimeMachine::now();
        std::vector<std::shared_ptr<SceneObject>> enemies = this->getAllObjectOfType<Enemy>();
        if (!enemies.empty())
        {
            auto ran = random(int(0), int(enemies.size()) - int(1));
            auto enemyPick = enemies.at(ran);
            shootWeapon(enemyPick->getMatrix(), Weapons::Shooter::ENEMY);
        }
    }
    else if (duration_cast<milliseconds>(TimeMachine::now() - lastEnemyFire).count() > nextFireFromEnemy)
    {
        enemyFire_flag = false;
    }
}

void GameManager::ufoFire()
{
    if (!ufoFire_flag)
    {
        if(wave3) nextFireFromUFO = 400;
        if(wave4) nextFireFromUFO = 300;
        ufoFire_flag = true;
        lastUFOFire = TimeMachine::now();
        std::vector<std::shared_ptr<SceneObject>> ufos = this->getAllObjectOfType<SpaceObject>();
        if (!ufos.empty())
        {
            auto ran = random(int(0), int(ufos.size()) - int(1));
            auto ufoPick = ufos.at(ran);
            shootWeapon(ufoPick->getMatrix(), Weapons::Shooter::UFO);
        }
    }
    else if (duration_cast<milliseconds>(TimeMachine::now() - lastUFOFire).count() > nextFireFromUFO)
    {
        ufoFire_flag = false;
    }
}

void GameManager::SpaceshipFire()
{
    if (!spaceshipFire_flag)
    {
        spaceshipFire_flag = true;
        lastFire = TimeMachine::now();
        auto spaceshipmatrix = spaceship_->getMatrix();
        auto currentWeapon = spaceship_->getWeapon();

        switch (currentWeapon)
        {
            case 0:
                shootWeapon(spaceshipmatrix, Weapons::Shooter::SPACESHIP);
                break;

            case 1:
                spaceshipmatrix[3].x -= 10;
                shootWeapon(spaceshipmatrix, Weapons::Shooter::SPACESHIP);

                spaceshipmatrix[3].x += 20;
                shootWeapon(spaceshipmatrix, Weapons::Shooter::SPACESHIP);
                break;

            case 2:
                shootWeapon(spaceshipmatrix, Weapons::Shooter::SPACESHIP);

                spaceshipmatrix[3].x -= 10;
                spaceshipmatrix = glm::rotate(spaceshipmatrix, 0.2f, glm::vec3(0.0f, 1.0f, 0.0f));
                shootWeapon(spaceshipmatrix, Weapons::Shooter::SPACESHIP);

                spaceshipmatrix[3].x += 20;
                spaceshipmatrix = glm::rotate(spaceshipmatrix, 0.4f, glm::vec3(0.0f, -1.0f, 0.0f));
                shootWeapon(spaceshipmatrix, Weapons::Shooter::SPACESHIP);
                break;

            case 3:
                shootWeapon(spaceshipmatrix, Weapons::Shooter::SPACESHIP);

                spaceshipmatrix[3].x -= 10;
                shootWeapon(spaceshipmatrix, Weapons::Shooter::SPACESHIP);

                spaceshipmatrix[3].x += 20;
                shootWeapon(spaceshipmatrix, Weapons::Shooter::SPACESHIP);

                spaceshipmatrix[3].x -= 20;
                spaceshipmatrix = glm::rotate(spaceshipmatrix, 0.2f, glm::vec3(0.0f, 1.0f, 0.0f));
                shootWeapon(spaceshipmatrix, Weapons::Shooter::SPACESHIP);

                spaceshipmatrix[3].x += 20;
                spaceshipmatrix = glm::rotate(spaceshipmatrix, 0.4f, glm::vec3(0.0f, -1.0f, 0.0f));
                shootWeapon(spaceshipmatrix, Weapons::Shooter::SPACESHIP);
                break;

            default:
                break;
        }
    }
    else if (duration_cast<milliseconds>(TimeMachine::now() - lastFire).count() > nextFireFromSpaceship)
    {
        spaceshipFire_flag = false;
    }
}

void GameManager::shootWeapon(glm::mat4 &object_matrix,
                              Weapons::Shooter s)
{
    if (s == Weapons::Shooter::SPACESHIP){
        weapons_.reset(new Weapons(object_matrix, Weapons::Shooter::SPACESHIP, 10.0f));
        this->addSubObject(weapons_);
        player_shoots_array.push_back(weapons_);
        playSound(Sound::BULLET);
    }
    else if(s == Weapons::Shooter::ENEMY){
        weapons_.reset(new Weapons(object_matrix, Weapons::Shooter::ENEMY, 5.0f));
        this->addSubObject(weapons_);
        enemy_shoots_array.push_back(weapons_);
    }
    else if(s == Weapons::Shooter::UFO){
        weapons_.reset(new Weapons(object_matrix, Weapons::Shooter::UFO, 5.0f));
        this->addSubObject(weapons_);
        ufo_shoots_array.push_back(weapons_);
    }
}

void GameManager::removeLastGeneratedParticles()
{
    if (particle_generators.size() >= 7)
    {
        this->removeSubObject(particle_generators[0]);
        particle_generators.erase(particle_generators.begin());
    }
}

void GameManager::shootingCleanup()
{
    for (auto shot_it = player_shoots_array.begin(); shot_it != player_shoots_array.end();)
    {
        auto player_shot = *(shot_it);
        auto z_value = player_shot->getMatrix()[3].z;
        if (z_value < -3000 && player_shot->_trajectory == Weapons::Shooter::SPACESHIP) {
            this->removeSubObject(player_shot);
            shot_it = player_shoots_array.erase(shot_it);
        }
        else {
            shot_it++;
        }
    }

    for (auto shot_it = enemy_shoots_array.begin(); shot_it != enemy_shoots_array.end();)
    {
        auto enemy_shot = *(shot_it);
        auto z = enemy_shot->getMatrix()[3].z;
        if (z < -3000 && enemy_shot->_trajectory == Weapons::Shooter::ENEMY) {
            this->removeSubObject(enemy_shot);
            shot_it = enemy_shoots_array.erase(shot_it);
        }
        else {
            shot_it++;
        }
    }

    for (auto shot_it = ufo_shoots_array.begin(); shot_it != ufo_shoots_array.end();)
    {
        auto ufo_shot = *(shot_it);
        auto y = ufo_shot->getMatrix()[3].y;
        if (y < -100 && ufo_shot->_trajectory == Weapons::Shooter::UFO) {
            this->removeSubObject(ufo_shot);
            shot_it = ufo_shoots_array.erase(shot_it);
        }
        else {
            shot_it++;
        }
    }
}

void GameManager::powerupCleanup()
{
    for (auto power_it = powerup_array.begin(); power_it != powerup_array.end();)
    {
        auto powerup = *(power_it);
        if (powerup->getMatrix()[3].z > 100)
        {
            this->removeSubObject(powerup);
            power_it = powerup_array.erase(power_it);
        }
        else {
            power_it++;
        }
    }
}

void GameManager::enemyCleanup()
{
    for (auto ship_it = enemies_array.begin(); ship_it != enemies_array.end();)
    {
        auto enemy_ship = *(ship_it);
        if (enemy_ship->getMatrix()[3].z > 100)
        {
            this->removeSubObject(enemy_ship);
            ship_it = enemies_array.erase(ship_it);
        }
        else {
            ship_it++;
        }
    }
}

void GameManager::ufoCleanup()
{
    for (auto ship_it = ufos_array.begin(); ship_it != ufos_array.end();)
    {
        auto ufo_ship = *(ship_it);
        if (ufo_ship->getMatrix()[3].z > 100 or ufo_ship->getMatrix()[3].z < -1000)
        {
            this->removeSubObject(ufo_ship);
            ship_it = ufos_array.erase(ship_it);
        }
        else {
            ship_it++;
        }
    }
    if(ufos_array.size() == 0)
        generateUFO_flag = false;
}

void GameManager::enemyShootPlayer()
{
    for (auto shot_it = enemy_shoots_array.begin(); shot_it != enemy_shoots_array.end();)
    {
        auto enemy_shot = *(shot_it);
        if (detectCollision(spaceship_->getMatrix()[3],
                            10.0f,
                            enemy_shot->getMatrix()[3],
                            5.0f))
        {
            playSound(Sound::HIT);
            this->removeSubObject(enemy_shot);
            shot_it = enemy_shoots_array.erase(shot_it);

            this->spaceship_->life_ -= enemy_shot->bullet_power;

            //Check For Failure!
            if (spaceship_->life_ < 1){
                playSound(Sound::GAMEOVER);
                this->removeSubObject(spaceship_);
                gameOver = true; this->spaceship_->life_= 0;
            }
        }
        else {
            shot_it++;
        }
    }
}

void GameManager::playerShootEnemy()
{
    bool hit = false;
    for (auto shot_it = player_shoots_array.begin(); shot_it != player_shoots_array.end();)
    {
        auto player_shot = *(shot_it);
        for (auto ship = enemies_array.begin(); ship != enemies_array.end(); ++ship)
        {
            auto enemy_ship = *(ship);
            if (detectCollision(enemy_ship->getMatrix()[3],
                                10.0f,
                                player_shot->getMatrix()[3],
                                5.0f))
            {
                this->removeSubObject(player_shot);
                shot_it = player_shoots_array.erase(shot_it);

                enemy_ship->life_ -= player_shot->bullet_power;

                // Destroy Enemy
                if (enemy_ship->life_ <= 0)
                {
                    playSound(Sound::EXPLOSION);
                    this->removeSubObject(enemy_ship);
                    enemies_array.erase(ship);
                    particles_.reset(new ParticleGenerator(particle_explosion_tex,
                                                           ParticleGenerator::Type::EXPLOSION,
                                                           weapons_));
                    this->addSubObject(particles_);
                    particle_generators.push_back(particles_);
                    particles_->getMatrix()[3] = enemy_ship->getMatrix()[3];
                    this->currentScore += 10.0;
                    //Add Power-Up
                    if (random(1, 50) < powerup_percentage)
                    {
                        powerup_.reset(new Powerup());
                        this->addSubObject(powerup_);
                        powerup_array.push_back(powerup_);
                        powerup_->getMatrix()[3] = enemy_ship->getMatrix()[3];
                    }

                    this->currentScore += 40;
                }
                hit = true;
                break;
            }
        }
        if (!hit) shot_it++;
        else hit = false;
    }
}

void GameManager::playerHitEnemy()
{
    for (auto ship_it = enemies_array.begin(); ship_it != enemies_array.end();)
    {
        auto enemy_ship = *(ship_it);
        if (detectCollision(spaceship_->getMatrix()[3],
                            10.0f,
                            enemy_ship->getMatrix()[3],
                            10.0f))
        {
            //Destroy Enemy
            playSound(Sound::EXPLOSION);
            this->removeSubObject(enemy_ship);
            ship_it = enemies_array.erase(ship_it);
            particles_.reset(new ParticleGenerator(particle_explosion_tex,
                                                   ParticleGenerator::Type::EXPLOSION,
                                                   weapons_));
            this->addSubObject(particles_);
            particle_generators.push_back(particles_);
            particles_->getMatrix()[3] = enemy_ship->getMatrix()[3];

            spaceship_->life_ -= 1.0;
            this->currentScore -= 15.0;
            //Check For Failure!
            if (spaceship_->life_ < 1){
                playSound(Sound::GAMEOVER);
                this->removeSubObject(spaceship_);
                gameOver = true; this->spaceship_->life_= 0;
            }
        }
        else {
            ship_it++;
        }
    }
}

void GameManager::playerPowerup()
{
    for (auto power_it = powerup_array.begin(); power_it != powerup_array.end();)
    {
        auto powerup = *(power_it);
        if (detectCollision(powerup->getMatrix()[3],
                            10.0f,
                            spaceship_->getMatrix()[3],
                            10.0f))
        {
            playSound(Sound::POWERUP);
            this->removeSubObject(powerup);
            power_it = powerup_array.erase(power_it);

            lastPowerup = TimeMachine::now();

            spaceship_->isPoweredUp = true;
            spaceship_->life_       += 1;

        }
        else {
            power_it++;
        }
    }
}

void GameManager::removePowerup(){
    if (powerup_flag)
    {
        powerup_flag = false;
        lastPowerup = TimeMachine::now();
    }
    else if (duration_cast<milliseconds>(TimeMachine::now() - lastPowerup).count() > nextPowerup)
    {
        this->getSpaceShip()->changeWeapon(0);
        this->getSpaceShip()->isPoweredUp = false;
        powerup_flag = true;
    }
}

double GameManager::getHighscore()
{
    return this->highScore;
}

double GameManager::getCurrentScore()
{
    return this->currentScore;
}

int GameManager::getSpaceshipLife()
{
    return this->spaceship_->getLife();
}

void GameManager::playSound(const GameManager::Sound &s)
{
    if (!SoundEngine)
    {
        std::cout <<"irrKlang ERROR: Could not startup the sound!"
                  << std::endl;
    }
    else if(s == Sound::EXPLOSION){
        SoundEngine->play2D(explosionSound);
    }
    else if(s == Sound::BULLET){
        SoundEngine->play2D(bulletSound);
    }
    else if(s == Sound::GAMEOVER){
        SoundEngine->play2D(gameoverSound);
    }
    else if(s == Sound::POWERUP){
        SoundEngine->play2D(powerupSound);
    }
    else if(s == Sound::HIT){
        SoundEngine->play2D(hitSound);
    }
}

std::shared_ptr<Camera> GameManager::getCam()
{
    return cam_;
}

std::shared_ptr<SpaceShip> GameManager::getSpaceShip()
{
  return spaceship_;
}
