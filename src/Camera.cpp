#include "../include/Camera.hpp"
#define PIOVER180 0.0174532925199

Camera::Camera()
{
  matrix_ = glm::translate(glm::mat4(1.0), glm::vec3(0.0f, 0.0f, 0.0f));
}

Camera::~Camera()
{
}

void Camera::privateInit()
{
}

void Camera::privateRender()
{
  // not drawing any camera geometry
}

void Camera::privateUpdate()
{
      matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, 0.3f));
}

void Camera::updateCamera()
{
}

void Camera::rotateLeft()
{
    matrix_ = glm::rotate(matrix_, 0.2f, glm::vec3(0.0f, -0.1f, 0.0f));
}

void Camera::rotateRight()
{
    matrix_ = glm::rotate(matrix_, 0.2f, glm::vec3(0.0f, 0.1f, 0.0f));
}

void Camera::rotateUp()
{
    matrix_ = glm::rotate(matrix_, 0.2f, glm::vec3(0.0f, 0.0f, -0.1f));
}

void Camera::rotateDown()
{
    matrix_ = glm::rotate(matrix_, 0.2f, glm::vec3(0.0f, 0.0f, 0.1f));
}

//void Camera::moveRight(/*const float& amount*/)
//{
//    matrix_ = glm::translate(matrix_, glm::vec3((speed_ * dt), 0.0f, 0.0f));
//}

//void Camera::moveLeft(/*const float& amount*/)
//{
//    matrix_ = glm::translate(matrix_, glm::vec3(-(speed_ * dt), 0.0f, 0.0f));
//}

//void Camera::moveUp()
//{
//    matrix_ = glm::translate(matrix_, glm::vec3(0.0f,(speed_ * dt), 0.0f));
//}

//void Camera::moveDown()
//{
//    matrix_ = glm::translate(matrix_, glm::vec3(0.0f,-(speed_ * dt), 0.0f));
//}

//void Camera::moveForward()
//{
//    matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f,(speed_ * dt)));
//}

//void Camera::moveBackward()
//{
//  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f,-(speed_ * dt)));
//}


void Camera::moveRight()
{
  matrix_ = glm::translate(matrix_, glm::vec3(-0.5f, 0.0f, 0.0f));
}
void Camera::moveLeft()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.5f, 0.0f, 0.0f));
}
void Camera::moveUp()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.25f, 0.0f));
}
void Camera::moveDown()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, -0.25f, 0.0f));
}
void Camera::moveForward()
{
    matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, 0.5f));
}
void Camera::moveBackward()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, -0.5f));
}
void Camera::mouseQuaternion(const int &diffX, const int &diffY)
{
}

