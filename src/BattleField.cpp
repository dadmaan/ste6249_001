#include "../include/BattleField.hpp"


BattleField::BattleField()
{
      matrix_ = glm::translate(glm::mat4(1.0), glm::vec3(-900.0f, 400.0f, 0.0f));
}

BattleField::~BattleField()
{
}

void BattleField::privateInit()
{
    _shader_field.initShaders("/home/daddy/Documents/OpenGL/STE6249/shaders/landscape");
    // Cube setup
    // Create vertex arrays
    auto const X_LENGTH = 50.0f, Z_LENGTH = 10.0f, y = -100.0f;
    for (int i = 0; i < rows; ++i)
    {
      for (int j = 0; j < columns; ++j)
      {
        auto x = j*X_LENGTH, z = i*Z_LENGTH;
        //Just one vertex per iteration!
        bf_struct.vertices.push_back(glm::vec3{x, y, -z});
        bf_struct.textureCoordinates.push_back(glm::vec2{x/(columns*X_LENGTH), z/(rows*Z_LENGTH)});

      }
    }

    //Sets up the indices we need for the trianglestrip
    for (int i = 1; i < rows; ++i)
    {
      for (int j = 0; j < columns; ++j)
      {
        const auto& val1 = i*32 + j;
        const auto& val2 = (i-1)*32+j;

        bf_struct.indices.push_back(val1);
        bf_struct.indices.push_back(val2);
      }
      bf_struct.indices.push_back(UINT_MAX);
    }

    //Loading Textures
    color_id_ = loadTexture("/home/daddy/Documents/OpenGL/STE6249/materials/maps/colorMap.bmp");
    height_id_ = loadTexture("/home/daddy/Documents/OpenGL/STE6249/materials/maps/heightMap.bmp");

    _shader_field.enable();

    GLint location;
    GLuint program = _shader_field.getProg();

    location = glGetUniformLocation(program, "colorMap");
    glUniform1i(location ,0);

    location = glGetUniformLocation(program, "heightMap");
    glUniform1i(location, 1);

    _shader_field.disable();
}

void BattleField::privateRender()
{

    _shader_field.enable();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, color_id_);
    glEnable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, height_id_);
    glEnable(GL_TEXTURE_2D);

    //Draw Field
    glEnable(GL_PRIMITIVE_RESTART);
    glPrimitiveRestartIndex(UINT_MAX);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &bf_struct.vertices[0]);
    glTexCoordPointer(2, GL_FLOAT, 0, &bf_struct.textureCoordinates[0]);
    glDrawElements(GL_TRIANGLE_STRIP,bf_struct.indices.size(),
                   GL_UNSIGNED_INT, &bf_struct.indices[0]);
    glDisable(GL_PRIMITIVE_RESTART);

    glActiveTexture(GL_TEXTURE1);
    glDisable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_2D);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);

    _shader_field.disable();
}

void BattleField::privateUpdate()
{
//    if (matrix_[3].z < 5120.0f)
//    {
//        matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, 450 * dt));
//    }
//    else {
//        matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, -4000 * 2));
//    }
}

