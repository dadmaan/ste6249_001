
This is a final project for the Virtual Reality-STE6249 course in openGL.
The project is impelemnted and developed in QT platform.


1. Install required packages:
*    	glm
*    	glu
*    	freeglut
*    	lib32-glu
*    	lib32-freeglut
*    	SOIL
*		IrrKlang


2. Sources:
*    	OBJ_Loader.h          	https://github.com/Bly7/OBJ-Loader  
*    	Objects/Textures      	https://www.turbosquid.com/
*		IrrKlang				https://www.ambiera.com/irrklang/


3. You need to set the new paths for followings: 
*	SceneObject.hpp: lines 116,117.
*	GameManager.cpp: lines 18,19,20.
*	BattleField.cpp: line  15.
*	Enemy.cpp:	 line  21.
*	Particles.cpp:	 line  27.
*	Powerup.cpp:	 line  13.
*	Skybox.cpp:	 line  14.
*	SpaceObject.cpp: line  24.
*	SpaceShip.cpp:	 lines 14,16.
*	Water.cpp:	 lines 15,61.
*	Weapons.cpp:	 line  26.

4. Check for ScreenShots in the project folder.
 