#pragma once

//#include <windows.h>
#include "SceneObject.hpp"

class Enemy : public SceneObject
{
    public:
        enum class Trajectory {FORWARD, RIGHT, LEFT};
        enum class Type {NORMAL};

        //Constructor
        Enemy(const DataStructure& object,
              const GLuint& texture,
              const glm::vec3& translate,
              const Trajectory& t);

        ~Enemy();

        float           speed_;
        float           life_ = 2;

    protected:
        void privateInit();
        void privateRender();
        void privateUpdate();

    private:
        DataStructure   object_enemy;
        Shader          shader_enemy;
        GLuint          tex_;
        Trajectory      trajectory_;

        float           position_x;
        float           position_z;
};

