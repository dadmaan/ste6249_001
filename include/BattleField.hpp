#pragma once

//#include <windows.h>
#include "SceneObject.hpp"

class BattleField : public SceneObject
{
	public:
		BattleField();
		~BattleField();

  protected:
        virtual void privateInit();
		virtual void privateRender();
		virtual void privateUpdate();

	private:
        int rows {512};
        int columns {32};
        DataStructure bf_struct;

        Shader _shader_field;
        GLuint height_id_;
        GLuint heightMap;
        GLuint color_id_;
        GLuint colorMap;

};

