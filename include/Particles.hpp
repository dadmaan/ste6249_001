#pragma once
#include "SceneObject.hpp"
#include "Weapons.hpp"


#define	MAX_PARTICLES	300
#define	MAX_PARTICLES_BULLET	200

class ParticleGenerator : public SceneObject
{
    public:
    enum class Type {EXPLOSION, BULLET};

    ParticleGenerator(GLuint& texture,
                      ParticleGenerator::Type genType,
                      std::shared_ptr<Weapons> weapons);
    ~ParticleGenerator();

    protected:
    void privateInit();
    void privateRender();
    void privateUpdate();

    private:
    void _init();
    void explosion();
    void bullet();

    typedef struct particles						// Create A Structure For Particle
    {
        bool	active;					// Active (Yes/No)
        float	life;					// Particle Life
        float	fade;					// Fade Speed
        float	r;						// Red Value
        float	g;						// Green Value
        float	b;						// Blue Value
        float	x;						// X Position
        float	y;						// Y Position
        float	z;						// Z Position
        float	xi;						// X Direction
        float	yi;						// Y Direction
        float	zi;						// Z Direction
        float	xg;						// X Gravity
        float	yg;						// Y Gravity
        float	zg;						// Z Gravity
    };							// Particles Structure

    std::shared_ptr<Weapons>    weapons_;
    ParticleGenerator::Type     type;

    particles                   particle[MAX_PARTICLES];
    GLfloat                     colors[12][3];
    GLuint                      tex_;
    Shader                      shader_particle;
    GLint                       fadeEffect;

    float                       explosion_slowdown  = 1.0f;
    float                       bullet_slowdown     = 100.0f;
    float                       y_speed;
    float                       x_speed;

    bool                        faded = false;
};
