#pragma once

//#include <windows.h>
#include "SceneObject.hpp"

class Camera : public SceneObject
{
	public:
		Camera();
		~Camera();

        void moveRight(/*const float& amount*/);
        void moveLeft(/*const float& amount*/);
        void moveUp();
        void moveDown();
        void moveBackward();
        void moveForward();
        void updateCamera();
        void rotateLeft();
        void rotateRight();
        void rotateUp();
        void rotateDown();
        void mouseQuaternion(const int& diffX, const int& diffY);

        float speed_ = 0.1f;
    
  protected:
        void privateInit();
		void privateRender();
		void privateUpdate();

};


