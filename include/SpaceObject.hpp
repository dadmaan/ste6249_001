#pragma once

//#include <windows.h>
#include "SceneObject.hpp"
#include "Camera.hpp"

class SpaceObject : public SceneObject
{
    public:
    enum class Trajectory {RIGHT, LEFT};
        SpaceObject(const DataStructure& object,
                    const GLuint& texture,
                    float pos_x,
                    float pos_z,
                    const Trajectory& t);
        ~SpaceObject();

  protected:
        void privateInit();
        void privateRender();
        void privateUpdate();

    private:
        DataStructure   object_spaceobject;
        GLuint          tex_;
        Shader          shader_spaceobject;
        Trajectory      trajectory_;


        std::shared_ptr<Camera>     cam_;

    public:
        bool UFO_move_flag          = false;
        float position_x;
        float position_z;
};

