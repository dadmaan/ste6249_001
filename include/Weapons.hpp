#pragma once
#include "SceneObject.hpp"

class Weapons : public SceneObject
{
        public:
        enum class Shooter {SPACESHIP, ENEMY, UFO};

         Weapons(const glm::mat4& startMatrix,
                 Shooter s,
                 float trajectoryRate);
        ~Weapons();

        float bullet_power = 1.0f;
        Shooter _trajectory = Shooter::SPACESHIP;
        float speed_ = 50.0f;

        protected:
        void privateInit();
        void privateRender();
        void privateUpdate();

        private:
        Shader shader_weapon;
};

