#pragma once

#include "irrKlang/include/irrKlang.h"
#include <GL/glew.h>
#include "glm/glm.hpp"
#include "SOIL/SOIL.h"
#include <GL/freeglut.h>
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/quaternion.hpp"
#include "Shader.hpp"

#include <algorithm>
#include <vector>
#include <memory>
#include <iostream>
#include <fstream>
#include <math.h>
#include <random>
#include <chrono>


// The SceneObject class is the parent of all objects in the scene graph.
// To make an object to use in the scene graph, inherit this class and
// override the methods private_init, private_update and private_render.
//
// The rest of the magic creating the scene graph is implemented in the
// methods init, update and render. Do NOT override those methods.
//
// The application is to create a "root" instance of SceneObject (or another
// class inheriting SceneObject). Then:
//  Call the root.init after OpenGL is initialized,
//  call root.update in the render-function, and
//  call root.render after update has been called.
//
// private_init: Here all the initializer-code is put, this method is called
//      after OpenGL has been initialized.
// private_update: This method is called prior to any rendering, animation-
//      code is to be implemented in here (motion, collition-detection). When
//      this method is called, the member sslf_ has been set, so time-dependent
//      animation can use this value.
// private_render: This is where the rendering-code is implemented. Here you
//      do all the actual drawing.
//
// matrix_: This is the transformation matrix of the scene-object. This
//      transformation is relative to the object's parent. The transformation
//      is performed as part of the scene graph-magic.
//
// The storage of the children is handled by smart pointers, this is because


using namespace irrklang;
class SceneObject
{
 public:
  SceneObject();
  virtual ~SceneObject();

  // This method causes all children to be rendered, don't override this one!
  void render();
  // This method causes all children to be updated, don't override this one!
  void update(double fps);
  // This method causes all children to be initialized,don't override this one!
  void init();

  void addSubObject(std::shared_ptr<SceneObject> newchild);
  void removeSubObject(const std::shared_ptr<SceneObject> child);

  template <typename T>
  std::vector<std::shared_ptr<SceneObject>> getAllObjectOfType();

  //Random Functions
  std::random_device RandomMachine;
  template <typename T>
  int random(const T & from, const T & to);

  template<typename T>
  int randomExcludeZero(const T & from, const T & to);

  // Dangerous to enable, and use SharedPtr if this is to be used!
  //  std::vector<ScopedPtr<SceneObject> >& getSubObjects();

  struct DataStructure {
      std::vector<glm::vec3> vertices;
      std::vector<glm::uint> indices;
      std::vector<glm::vec2> textureCoordinates;
      std::vector<glm::vec3> normals;
  };
  DataStructure loadObject(const char *file);
  unsigned int  loadTexture(const char *file);
  bool detectCollision(glm::vec3 object_a,
                       float ra,
                       glm::vec3 object_b,
                       float rb);
  bool tex_success = false;

  void setMatrix(const glm::mat4& m) { matrix_ = m; }
  glm::mat4& getMatrix() { return matrix_; }

 protected:
  // Override this method with your own render-implementation.
  virtual void privateRender() {}
  // Override this method with your own update-implementation.
  virtual void privateUpdate() {}
  // Override this method with your own init-implementation.
  virtual void privateInit() {}

  // This member contains the time since last frame. It is set
  // before privateUpdate is called.
  double fps_;
  double dt;

  // This is the transformation-matrix of the scene object.
  // Relative to the object's parent. Defaults to the identity matrix.
  glm::mat4 matrix_;

  std::string skybox_path_ = "/home/daddy/Documents/OpenGL/STE6249/materials/skybox/";
  std::string models_path = "/home/daddy/Documents/OpenGL/STE6249/materials/models/";
  float PI = 3.1415926535897932384626433832795f;
 private:
  // List of all SceneObjects that belong to the current object.
    std::vector<std::shared_ptr<SceneObject> > children_;
};

template<typename T>
inline std::vector<std::shared_ptr<SceneObject>> SceneObject::getAllObjectOfType()
{
    std::vector<std::shared_ptr<SceneObject>> objects;
    for (auto i : children_) {
        if (std::dynamic_pointer_cast<T>(i)) {
            objects.push_back(i);
        }
    }
    return objects;
};

template<typename T>
inline int SceneObject::random(const T & from, const T & to)
{
    std::mt19937 rng(RandomMachine());
    std::uniform_int_distribution<T> uni(from, to);
    return uni(rng);
}

template<typename T>
inline int SceneObject::randomExcludeZero(const T & from, const T & to)
{
    std::mt19937 rng(RandomMachine());
    std::uniform_int_distribution<T> uni(from, to);
    auto randomized = uni(rng);
    if (randomized == 0)
    {
        randomized = 1;
    }
    return randomized;
}
