#pragma once

//#include <windows.h>
#include "SceneObject.hpp"
#include "Camera.hpp"

class SpaceShip : public SceneObject
{
	public:
        SpaceShip(const DataStructure&      data);
        ~SpaceShip();

        int     getWeapon();
        int     getLife();
        void    changeWeapon(const int &weapon);

        void    moveRight();
        void    moveLeft();
        void    moveBackward();
        void    moveForward();

        bool    isPoweredUp = false;
        int     life_       = 3;

  protected:
        void privateInit();
        void privateRender();
        void privateUpdate();

	private:
        DataStructure   data_spaceship;
        Shader          shader_spaceship;
        GLuint          tex_;

        float           speed_  = 200.0f;
        float           armor_;
        int             weapon_ = 0;
    
};

