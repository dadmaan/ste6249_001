#pragma once

//#include <windows.h>
#include "SceneObject.hpp"
#include "BattleField.hpp"
#include "Water.hpp"
#include "SpaceShip.hpp"
#include "Enemy.hpp"
#include "SpaceObject.hpp"
#include "Skybox.hpp"
#include "Camera.hpp"
#include "Weapons.hpp"
#include "Particles.hpp"
#include "Powerup.hpp"
#include "Clock.hpp"

using namespace std::chrono;

class GameManager : public SceneObject
{
	public:
        //System Time Machine
        using TimeMachine   = high_resolution_clock;
        using TimePoint     = time_point<TimeMachine>;

        //Constructor
		GameManager();
        ~GameManager();

        //Functions
        void                        generateEnemy();
        void                        generateUFO();
        void                        EnemyFire();
        void                        ufoFire();
        void                        SpaceshipFire();
        void                        shootWeapon(glm::mat4 &object_matrix, Weapons::Shooter s);
        void                        removeLastGeneratedParticles();
        void                        shootingCleanup();
        void                        powerupCleanup();
        void                        enemyCleanup();
        void                        ufoCleanup();
        void                        enemyShootPlayer();
        void                        playerShootEnemy();
        void                        playerHitEnemy();
        void                        playerPowerup();
        void                        removePowerup();
        double                      getHighscore();
        double                      getCurrentScore();
        int                         getSpaceshipLife();

        std::shared_ptr<SpaceShip>  getSpaceShip();
        std::shared_ptr<Camera>     getCam();

        //Audio Settings
        enum class Sound {EXPLOSION, BULLET, GAMEOVER, POWERUP, HIT};
        void playSound(const Sound& s);
        irrklang::ISoundEngine* SoundEngine     = irrklang::createIrrKlangDevice();
        irrklang::ISoundSource* explosionSound  = SoundEngine->addSoundSourceFromFile("/home/daddy/Documents/OpenGL/STE6249/materials/audio/explosion.wav");
        irrklang::ISoundSource* bulletSound     = SoundEngine->addSoundSourceFromFile("/home/daddy/Documents/OpenGL/STE6249/materials/audio/single_shot.wav");
        irrklang::ISoundSource* gameoverSound   = SoundEngine->addSoundSourceFromFile("/home/daddy/Documents/OpenGL/STE6249/materials/audio/gameover.wav");
        irrklang::ISoundSource* powerupSound    = SoundEngine->addSoundSourceFromFile("/home/daddy/Documents/OpenGL/STE6249/materials/audio/powerup.wav");
        irrklang::ISoundSource* hitSound        = SoundEngine->addSoundSourceFromFile("/home/daddy/Documents/OpenGL/STE6249/materials/audio/hit.wav");
        irrklang::ISoundSource* theme           = SoundEngine->addSoundSourceFromFile("/home/daddy/Documents/OpenGL/STE6249/materials/audio/theme.wav");
        irrklang::ISoundSource* gameplay        = SoundEngine->addSoundSourceFromFile("/home/daddy/Documents/OpenGL/STE6249/materials/audio/alien.wav");

        //Data Structures
        std::vector<std::shared_ptr<Enemy>>             enemies_array;
        std::vector<std::shared_ptr<SpaceObject>>       ufos_array;
        std::vector<std::shared_ptr<Weapons>>           enemy_shoots_array;
        std::vector<std::shared_ptr<Weapons>>           ufo_shoots_array;
        std::vector<std::shared_ptr<Weapons>>           player_shoots_array;
        std::vector<std::shared_ptr<Powerup>>           powerup_array;
        std::vector<std::shared_ptr<ParticleGenerator>> particle_generators;
        std::vector<std::shared_ptr<ParticleGenerator>> particle_weapons;
    
        //Objects
        DataStructure               spaceship_obj_data;
        DataStructure               enemy_obj_data;
        DataStructure               enemy2_obj_data;
        DataStructure               spaceobject_obj_data;

        //Textures
        GLuint                      enemy_tex;
        GLuint                      spaceobject_tex;
        GLuint                      particle_explosion_tex;
        GLuint                      weapons_tex;

        TimePoint                   lastGeneratedEnemy;
        TimePoint                   lastGeneratedUFO;
        TimePoint                   lastFire;
        TimePoint                   lastEnemyFire;
        TimePoint                   lastUFOFire;
        TimePoint                   lastPowerup;

        bool gameStart              =   false;
        bool gameOver               =   false;
        bool generateEnemy_flag     =   false;
        bool generateUFO_flag       =   false;
        bool spaceshipFire_flag     =   false;
        bool enemyFire_flag         =   false;
        bool ufoFire_flag           =   false;
        bool powerup_flag           =   false;
        bool wave1                  =   false;
        bool wave2                  =   false;
        bool wave3                  =   false;
        bool wave4                  =   false;

        int nextFireFromSpaceship   =   100;
        int nextFireFromEnemy       =   300;
        int nextFireFromUFO         =   500;
        int enemyGenerationTimeSpan =   2500;
        int ufoGenerationTimeSpan   =   5000;
        int nextPowerup             =   10000;
        int powerup_percentage      =   25;
        double currentScore         =   0.0;
        double highScore            =   500.0;
        double game_difficulty      =   200.0;

    protected:
        virtual void privateInit();
		virtual void privateRender();
        virtual void privateUpdate();

    private:
        std::shared_ptr<BattleField>        bf_;
        std::shared_ptr<BattleField>        bf2_;
        std::shared_ptr<Water>              water_;
        std::shared_ptr<Water>              water2_;
        std::shared_ptr<SpaceShip>          spaceship_;
        std::shared_ptr<Enemy>              enemy_;
        std::shared_ptr<SpaceObject>        spaceobject_;
        std::shared_ptr<Skybox>             skybox_;
        std::shared_ptr<Camera>             cam_;
        std::shared_ptr<Weapons>            weapons_;
        std::shared_ptr<ParticleGenerator>  particles_;
        std::shared_ptr<Powerup>            powerup_;
        siut::Clock                         clock_;

};
