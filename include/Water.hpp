#pragma once

//#include <windows.h>
#include "SceneObject.hpp"


class Water : public SceneObject
{
    public:
        Water();
        ~Water();

    protected:
        virtual void privateInit();
        virtual void privateRender();
        virtual void privateUpdate();

    private:
        int             rows {128};
        int             columns {128};
        float           waterLevel = 0.0f;


        DataStructure   w_struct;
        Shader          _shader_water;
        GLuint          water_id_;
        GLuint          waterMap;
};

