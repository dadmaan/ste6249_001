#pragma once

#include "SceneObject.hpp"

class Skybox : public SceneObject {

  public:
  Skybox();
  ~Skybox();

  GLuint    getSkybox();
  void      setPos(const glm::vec3 &vec);

  protected:
  virtual void privateInit();
  virtual void privateRender();
  virtual void privateUpdate();

  private:
  GLuint    skybox_id_;
  GLuint    skyBoxMap;
  Shader    shader_skybox;

  //Cube Vertex Coordinates
  float size = 1.0f;
  std::vector<float> vertexArray_ = {
        -size,-size,-size, //0  (0,0,0) //frontside
        -size, size,-size, //1  (0,1,0)

         size, size,-size, //2  (1,1,0)
         size,-size,-size, //3  (1,0,0)

         size,-size, size, //4  (1,0,1) //backside
         size, size, size, //5  (1,1,1)

        -size,-size, size, //6  (0,0,1)
        -size, size, size, //7  (0,1,1)
  };

  //Cube index Coordinates
  std::vector<int> indexArray_ = {

        //Front
        0, 2, 1,
        2, 0, 3,

        //West
        2, 3, 4,
        5, 2, 4,

        //Back
        4, 6, 5,
        5, 6, 7,

        //East
        0, 7, 6,
        1, 7, 0,

        //Bottom
        0, 6, 3,
        6, 4, 3,

         //up
         7, 2, 5,
         1, 2, 7,

  };
};
