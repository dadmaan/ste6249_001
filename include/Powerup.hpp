#pragma once
#include "include/SceneObject.hpp"



class Powerup : public SceneObject
{
public:


public:
    Powerup();
    ~Powerup();

    void privateInit();
    void privateRender();
    void privateUpdate();
    bool markedForDeletion();

    float speed_ = 100.0f;

private:
    bool _markDelete = false;
    Shader shader_powerup;


};
