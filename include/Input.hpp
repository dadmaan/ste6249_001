#pragma once

//Move the Spaceship
const int KEY_ID_W                      = 0;
const int KEY_ID_S                      = 1;
const int KEY_ID_A                      = 2;
const int KEY_ID_D                      = 3;
//Shooting
const int KEY_ID_SPACE                  = 4;
//Move Camera up and down
const int KEY_ID_C                      = 5;
const int KEY_ID_X                      = 6;
//Camera Rotation
const int KEY_ID_LEFTARROW              = 7;
const int KEY_ID_RIGHTARROW             = 8;
const int KEY_ID_UPARROW                = 9;
const int KEY_ID_DOWNARROW              = 10;
//...
const int KEY_ID_Z                      = 11;
const int KEY_ID_H                      = 12;
const int KEY_ID_J                      = 13;
const int KEY_ID_U                      = 14;
const int KEY_ID_N                      = 15;
//Start the Game
const int KEY_ID_P                      = 16;
//Reset the Game
const int KEY_ID_R                      = 17;
//Switch Between the Weapons
const int KEY_ID_1                      = 18;
const int KEY_ID_2                      = 19;
const int KEY_ID_3                      = 20;
const int KEY_ID_4                      = 21;

const int MOUSE_LEFT_BUTTON_DOWN        = 22;
