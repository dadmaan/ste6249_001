uniform sampler2D heightMap;
void main()
{
    vec4 position = gl_Vertex;
    gl_TexCoord[0] = gl_MultiTexCoord0;
    position.y = texture2D(heightMap, gl_TexCoord[0].xy).x * 2000.0 - 2000.0;
    gl_Position = gl_ModelViewProjectionMatrix * position;
}
