uniform samplerCube  waterMap;
varying vec4 texCoord;

varying vec3 normal;
varying mat4 view;
varying vec4 eye;

void main()
{

        vec3 eyeCoord = vec3(eye);
        eyeCoord.x = -eyeCoord.x;

        vec3 reflection = reflect(-normalize(eyeCoord), normal);

        gl_FragColor = textureCube(waterMap, reflection); /** texture2D(tex_colormap2, texCoord.xy)*/;
        gl_FragColor.a = 0.9;
}
