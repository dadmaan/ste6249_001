

###############################################################################
# Minimum CMake version required
cmake_minimum_required(VERSION 2.8)

# Global name of the project
project(SpaceInvaders)

###############################################################################
# Include required packages
find_package(Qt5Core REQUIRED)

#add_subdirectory(unittests)

###############################################################################
# Compile and build
set( INC_SRCS_PREFIX    include)
set( SRC_SRCS_PREFIX    src)
set( SRCS_PREFIX        app)
set( SH_SRCS_PREFIX     shaders)
set( M_SRCS_PREFIX      materials)

  # Source files and friends
  set( HDRS

      ${INC_SRCS_PREFIX}/BattleField.hpp
      ${INC_SRCS_PREFIX}/Camera.hpp
      ${INC_SRCS_PREFIX}/Clock.hpp
      ${INC_SRCS_PREFIX}/FpsCounter.hpp
      ${INC_SRCS_PREFIX}/GameManager.hpp
      ${INC_SRCS_PREFIX}/Input.hpp
      ${INC_SRCS_PREFIX}/SceneObject.hpp
      ${INC_SRCS_PREFIX}/SpaceShip.hpp
      ${INC_SRCS_PREFIX}/Enemy.hpp
      ${INC_SRCS_PREFIX}/SpaceObject.hpp
      ${INC_SRCS_PREFIX}/Shader.hpp
      ${INC_SRCS_PREFIX}/Skybox.hpp
      ${INC_SRCS_PREFIX}/Water.hpp
      ${INC_SRCS_PREFIX}/Weapons.hpp
      ${INC_SRCS_PREFIX}/Particles.hpp
      ${INC_SRCS_PREFIX}/Powerup.hpp
      ${INC_SRCS_PREFIX}/Audio.hpp
      ${INC_SRCS_PREFIX}/Text.hpp
      ${INC_SRCS_PREFIX}/Vector3.hpp

      ${INC_SRCS_PREFIX}/OBJ_Loader.h
      ${INC_SRCS_PREFIX}/irrKlang/include/irrKlang.h
        )

  set( SRCS
      ${SRC_SRCS_PREFIX}/BattleField.cpp
      ${SRC_SRCS_PREFIX}/Camera.cpp
      ${SRC_SRCS_PREFIX}/GameManager.cpp
      ${SRC_SRCS_PREFIX}/SceneObject.cpp
      ${SRC_SRCS_PREFIX}/Shader.cpp
      ${SRC_SRCS_PREFIX}/SpaceShip.cpp
      ${SRC_SRCS_PREFIX}/Enemy.cpp
      ${SRC_SRCS_PREFIX}/SpaceObject.cpp
      ${SRC_SRCS_PREFIX}/Skybox.cpp
      ${SRC_SRCS_PREFIX}/Water.cpp
      ${SRC_SRCS_PREFIX}/Particles.cpp
      ${SRC_SRCS_PREFIX}/Powerup.cpp
      ${SRC_SRCS_PREFIX}/Weapons.cpp
      ${SRC_SRCS_PREFIX}/Audio.cpp
      ${SRC_SRCS_PREFIX}/Text.cpp

      ${SRCS_PREFIX}/main.cpp
        )
  set( SH
      ${SH_SRCS_PREFIX}/red.vert
      ${SH_SRCS_PREFIX}/red.frag
      ${SH_SRCS_PREFIX}/reflection.vert
      ${SH_SRCS_PREFIX}/reflection.frag
      ${SH_SRCS_PREFIX}/brick.vert
      ${SH_SRCS_PREFIX}/brick.frag
      ${SH_SRCS_PREFIX}/phong.vert
      ${SH_SRCS_PREFIX}/phong.frag
      ${SH_SRCS_PREFIX}/phongshader.frag
      ${SH_SRCS_PREFIX}/phongshader.vert
      ${SH_SRCS_PREFIX}/fixed.vert
      ${SH_SRCS_PREFIX}/fixed.frag
      ${SH_SRCS_PREFIX}/landscape.vert
      ${SH_SRCS_PREFIX}/landscape.frag
      ${SH_SRCS_PREFIX}/skybox.vert
      ${SH_SRCS_PREFIX}/skybox.frag
      ${SH_SRCS_PREFIX}/water.vert
      ${SH_SRCS_PREFIX}/water.frag
      ${SH_SRCS_PREFIX}/spaceship.vert
      ${SH_SRCS_PREFIX}/spaceship.frag
      ${SH_SRCS_PREFIX}/particle.vert
      ${SH_SRCS_PREFIX}/particle.frag
      ${SH_SRCS_PREFIX}/ufo.vert
      ${SH_SRCS_PREFIX}/ufo.frag
        )

  set( M
      ${M_SRCS_PREFIX}/color_height/heightmap.bmp
      ${M_SRCS_PREFIX}/color_height/color_height.bmp

      ${M_SRCS_PREFIX}/maps/heightMap.bmp
      ${M_SRCS_PREFIX}/maps/normalMap.bmp
      ${M_SRCS_PREFIX}/maps/colorMap.bmp
      ${M_SRCS_PREFIX}/maps/lightMap.bmp

      ${M_SRCS_PREFIX}/skybox/down.bmp
      ${M_SRCS_PREFIX}/skybox/east.bmp
      ${M_SRCS_PREFIX}/skybox/north.bmp
      ${M_SRCS_PREFIX}/skybox/south.bmp
      ${M_SRCS_PREFIX}/skybox/up.bmp
      ${M_SRCS_PREFIX}/skybox/west.bmp

      ${M_SRCS_PREFIX}/models/spaceship/viper/viper.png

      ${M_SRCS_PREFIX}/models/enemy
      ${M_SRCS_PREFIX}/models/enemy
      ${M_SRCS_PREFIX}/models/enemy
      ${M_SRCS_PREFIX}/models/enemy
      ${M_SRCS_PREFIX}/models/enemy

      ${M_SRCS_PREFIX}/models/spaceobject
      ${M_SRCS_PREFIX}/models/spaceobject
      ${M_SRCS_PREFIX}/models/spaceobject
      ${M_SRCS_PREFIX}/models/spaceobject
      ${M_SRCS_PREFIX}/models/spaceobject
        )

###########################
# Define target: executable
add_executable( ${PROJECT_NAME}
  ${HDRS}
  ${SRCS}
  ${SH}
  ${M})

# Find IrrKlang header and library

# This module defines the following uncached variables:
#  IrrKlang_FOUND, if false, do not try to use IrrKlang
#  IrrKlang_INCLUDE_DIRS, where to find irrKlang.h.
#  IrrKlang_LIBRARIES, the libraries to link against to use IrrKlang
#  IrrKlang_LIBRARY_DIRS, the directory where the IrrKlang library is found.

SET(IrrKlang_FOUND OFF)

FIND_PATH(
  IrrKlang_INCLUDE_DIR
                irrKlang.h
                /usr/local/include
                /home/daddy/Documents/OpenGL/STE6249/include/irrKlang/include
)

IF(IrrKlang_INCLUDE_DIR)
  find_library(IrrKlang_LIBRARY
    NAMES IrrKlang
    PATHS /usr/local/lib /usr/lib
        )

  if(IrrKlang_LIBRARY)
    get_filename_component(IrrKlang_LIBRARY_DIRS ${IrrKlang_LIBRARY} PATH)
    # Set uncached variables as per standard.
    set(IrrKlang_FOUND ON)
    set(IrrKlang_INCLUDE_DIRS ${IrrKlang_INCLUDE_DIR})
    set(IrrKlang_LIBRARIES ${IrrKlang_LIBRARY})
  endif(IrrKlang_LIBRARY)
endif(IrrKlang_INCLUDE_DIR)

if(IrrKlang_FOUND)
  if(NOT IrrKlang_FIND_QUIETLY)
    message(STATUS "FindIrrKlang: Found both IrrKlang headers and library")
  endif(NOT IrrKlang_FIND_QUIETLY)
else(IrrKlang_FOUND)
  if(IrrKlang_FIND_REQUIRED)
    message(FATAL_ERROR "FindIrrKlang: Could not find IrrKlang headers or library")
  endif(IrrKlang_FIND_REQUIRED)
endif(IrrKlang_FOUND)

MARK_AS_ADVANCED(
  IrrKlang_INCLUDE_DIR
  IrrKlang_LIBRARY
)
#############################################################
find_package(glm REQUIRED)
if(glm_FOUND)
  # glm interface_link_libraries
  # get_target_property(glm_interface_include_directories glm::glm INTERFACE_INCLUDE_DIRECTORIES)
  # target_include_directories(${PROJECT_NAME} INTERFACE ${glm_interface_include_directories})
endif()

#find_package(freeglut REQUIRED)
#if(freeglut_FOUND)
#  include_directories( ${FREEGLUT_INCLUDE_DIRS} )
#  target_link_libraries( ${FREEGLUT_LIBRARY} )
#  # freeglut interface_link_libraries
#  # get_target_property(freeglut_interface_include_directories freeglut::freeglut INTERFACE_INCLUDE_DIRECTORIES)
#  # target_include_directories(${PROJECT_NAME} INTERFACE ${freeglut_interface_include_directories})
#endif()

#############################################################################
# Default from QtCreator
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
#############################################################################
# Configure linking
target_link_libraries( ${PROJECT_NAME}
  glm ${GLUT_glut_LIBRARY}
  GLEW GL GLU glut SOIL ${IrrKlang_LIBRARY})



