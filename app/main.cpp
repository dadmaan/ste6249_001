
//#include <windows.h>
#include <GL/glew.h>
#include <GL/freeglut.h>  
#include <stdlib.h>
#include <stdio.h>
#include <cstdio>
#include <vector>
#include <memory>
#include "../include/Input.hpp"
#include "../include/FpsCounter.hpp"
#include "../include/Clock.hpp"
#include "../include/GameManager.hpp"
#include "../include/Shader.hpp"
#include "../include/Text.hpp"
#include "glm/glm.hpp"

#include <iostream>
#include <sstream>
#include <cstring>
#include <stdio.h>

std::shared_ptr<GameManager> gm;
siut::FpsCounter counter;
siut::Clock _clock;

//Texts
Text _text;
Text _textStart;
Text _highScore;
Text _currentScore;
std::vector<std::pair<double, std::string>> score_list = {{300,"Allan"}, {500, "Jakob"}, {1200,"Jan"}, {5800, "Sondre"}, {460, "Ruben"}, {250, "William"}};

//Flags
bool text_intro_flag;
bool times_up_flag;
bool sort_flag;
bool score_entry_flag;

int window;

bool keyPressed[30];
int mousePosX, mousePosY; 
float moveX, moveY;

void prepareGame()
{
    gm.reset(new GameManager());
    gm->init();
    _text.init();

    for(int i=0; i<30; i++)
      keyPressed[i]=false;

    text_intro_flag = true;
    gm->SoundEngine->play2D(gm->theme);
}

void init()
{
  glClearColor(0.0, 0.0, 0.0, 0.0);
  glShadeModel(GL_SMOOTH);
  glEnable(GL_DEPTH_TEST);

  GLenum err = glewInit();
  if (GLEW_OK != err)
  {
      fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
  }
  counter.start();
  prepareGame();
}

void printText(const std::string&   text,
               const glm::vec3&     pos,
               const glm::vec3&     color,
               int f)
{
    char buffer[200];

    glColor3f(color.x, color.y, color.z);

    strcpy(buffer, text.c_str());
    _text.setPos(pos.x, pos.y, pos.z);
    if(f == 0)_text.printString(buffer, Text::FONT_NORMAL);
    if(f == 1)_text.printString(buffer, Text::FONT_SMALL);

    glClearColor(0, 0, 0, 1);
}
bool sortinrev(const std::pair<int, std::string> &a,
               const std::pair<int, std::string> &b)
{
    return (a.first > b.first);
}
void displayScore()
{
    int lh = 3;
    if(!sort_flag){
        sort_flag = true;
        std::sort(score_list.begin(), score_list.end(), sortinrev);
    }

    for(int i = 0; i < int(score_list.size()); i++){
        auto s = score_list.at(i);
        std::ostringstream score_;
        score_ << s.first;
        std::ostringstream name_;
        name_ << s.second;

        std::string name_str = name_.str();
        std::string score_str = score_.str();

        printText("RECORDS",
                          glm::vec3(-10.0f, 30.0f, -100.0f),
                          glm::vec3(1.0f, 1.0f, 1.0f),
                          0);
        printText(name_str + " : " + score_str,
                          glm::vec3(-10.0f, (30.0f-lh), -100.0f),
                          glm::vec3(1.0f, 1.0f, 1.0f),
                          0);
        lh+=3;
    }
}

void start() {
    score_entry_flag = false;
    sort_flag = false;
    gm->SoundEngine->stopAllSoundsOfSoundSource(gm->theme);
    gm->SoundEngine->play2D(gm->gameplay);
    gm->gameStart = true;
    gm->wave1 = true;
    text_intro_flag = false;
    _clock.start();
    keyPressed[KEY_ID_P] = false;
}
void reset(){
    gm->SoundEngine->stopAllSoundsOfSoundSource(gm->gameplay);
    times_up_flag = false;
    counter.restart();
    _clock.stop();
    _clock.reset();
    prepareGame();
}
void display()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  gm->update(counter.fps());
  gm->render();

  if(keyPressed[KEY_ID_W]==true)        {gm->getCam()->moveForward();         gm->getSpaceShip()->moveForward();}
  if(keyPressed[KEY_ID_A]==true)        {gm->getSpaceShip()->moveLeft();      gm->getCam()->moveLeft();}
  if(keyPressed[KEY_ID_D]==true)        {gm->getSpaceShip()->moveRight();     gm->getCam()->moveRight();}
  if(keyPressed[KEY_ID_S]==true)        {gm->getSpaceShip()->moveBackward();  gm->getCam()->moveBackward();gm->getSpaceShip()->moveBackward();}
  if(keyPressed[KEY_ID_X]==true)        {gm->getCam()->moveUp();}
  if(keyPressed[KEY_ID_Z]==true)        {gm->getCam()->moveDown();}
  if(keyPressed[KEY_ID_SPACE] == true)  {gm->SpaceshipFire();                 keyPressed[KEY_ID_SPACE] = false;}
  if(keyPressed[KEY_ID_1] == true)      {gm->getSpaceShip()->changeWeapon(0); keyPressed[KEY_ID_1] = false;}
  if(keyPressed[KEY_ID_2] == true)      {gm->getSpaceShip()->changeWeapon(1); keyPressed[KEY_ID_2] = false;}
  if(keyPressed[KEY_ID_3] == true)      {gm->getSpaceShip()->changeWeapon(2); keyPressed[KEY_ID_3] = false;}
  if(keyPressed[KEY_ID_4] == true)      {gm->getSpaceShip()->changeWeapon(3); keyPressed[KEY_ID_4] = false;}
  if(keyPressed[KEY_ID_P] == true)      {start();}
  if(keyPressed[KEY_ID_R] == true)      {reset();}
  if(text_intro_flag)
  {
      printText("PRESS -P- TO START THE GAME",
                        glm::vec3(-40.0f, 40.0f, -100.0f),
                        glm::vec3(1.0f, 1.0f, 1.0f),
                        0);
      printText("USE W-S-A-D TO MOVING AROUND",
                        glm::vec3(-40.0f, 36.0f, -100.0f),
                        glm::vec3(1.0f, 1.0f, 1.0f),
                        0);
      printText("USE X-Z TO MOVE THE CAMERA UP/DOWN",
                        glm::vec3(-40.0f, 32.0f, -100.0f),
                        glm::vec3(1.0f, 1.0f, 1.0f),
                        0);
      printText("PRESS SPACE-BAR TO START THE FIRE",
                        glm::vec3(-40.0f, 28.0f, -100.0f),
                        glm::vec3(1.0f, 1.0f, 1.0f),
                        0);
      printText("USE 1-2-3-4 TO SWITCH BETWEEN WEAPONS",
                        glm::vec3(-40.0f, 24.0f, -100.0f),
                        glm::vec3(1.0f, 1.0f, 1.0f),
                        0);
      printText("PRESS -R- TO RESTART THE GAME",
                        glm::vec3(-40.0f, 20.0f, -100.0f),
                        glm::vec3(1.0f, 1.0f, 1.0f),
                        0);
      printText("PRESS -Q- TO QUIT THE GAME",
                        glm::vec3(-40.0f, 16.0f, -100.0f),
                        glm::vec3(1.0f, 1.0f, 1.0f),
                        0);
      printText("-MAY THE FOURTH BE WITH YOU-",
                        glm::vec3(-20.0f, 8.0f, -100.0f),
                        glm::vec3(1.0f, 0.0f, 0.0f),
                        0);
  }
  // Switching between enemy waves
  if(gm->gameStart == true and _clock.elapsed() > 10.0 and gm->wave1 == true)
  {
      printText("GET READY FOR WAVE 2!!",
                        glm::vec3(-10.0f, -20.0f, -100.0f),
                        glm::vec3(1.0f, 1.0f, 1.0f),
                        0);
  }
  if(gm->gameStart == true and _clock.elapsed() > 15.0 and gm->wave1 == true)
  {
      gm->wave1 = false;
      gm->wave2 = true;
  }
  if(gm->gameStart == true and _clock.elapsed() > 20.0 and gm->wave2 == true)
  {
      printText("GET READY FOR WAVE 3!!",
                        glm::vec3(-10.0f, -20.0f, -100.0f),
                        glm::vec3(1.0f, 1.0f, 1.0f),
                        0);
  }
  if(gm->gameStart == true and _clock.elapsed() > 25.0 and gm->wave2 == true)
  {
      gm->wave1 = false;
      gm->wave2 = false;
      gm->wave3 = true;
  }
  if(gm->gameStart == true and _clock.elapsed() > 30.0 and gm->wave3 == true)
  {
      printText("GET READY FOR WAVE 4!!",
                        glm::vec3(-10.0f, -20.0f, -100.0f),
                        glm::vec3(1.0f, 1.0f, 1.0f),
                        0);
  }
  if(gm->gameStart == true and _clock.elapsed() > 35.0 and gm->wave3 == true)
  {
      gm->wave1 = false;
      gm->wave2 = false;
      gm->wave3 = false;
      gm->wave4 = true;
  }
  //End of the Game
  if(times_up_flag or gm->gameOver or (gm->gameStart == true and _clock.elapsed() > 45.0 and gm->wave4 == true))
  {
      if(!score_entry_flag)
      {
          score_entry_flag = true;
          score_list.push_back(std::make_pair(gm->currentScore, "Current Player"));
      }
      if(gm->gameOver){
          printText("GAME OVER!!",
                            glm::vec3(-10.0f, -20.0f, -100.0f),
                            glm::vec3(1.0f, 1.0f, 1.0f),
                            0);
          printText("PRESS -R- TO RESTART THE GAME",
                            glm::vec3(-23.0f, -25.0f, -100.0f),
                            glm::vec3(1.0f, 1.0f, 1.0f),
                            0);
          gm->gameStart = false;
      }
      else {
          gm->wave4 = false;
          times_up_flag = true;
          printText("TIMES UP!!",
                            glm::vec3(-10.0f, -20.0f, -100.0f),
                            glm::vec3(1.0f, 1.0f, 1.0f),
                            0);
          printText("PRESS -R- TO RESTART THE GAME",
                            glm::vec3(-23.0f, -25.0f, -100.0f),
                            glm::vec3(1.0f, 1.0f, 1.0f),
                            0);
          gm->gameStart = false;
      }
  }
  // Clear screen for new start
  if(gm->gameOver or times_up_flag)
  {
      auto enemies = gm->getAllObjectOfType<Enemy>();
      for (auto& e: enemies) {
          gm->removeSubObject(e);
      }

      auto bullets = gm->getAllObjectOfType<Weapons>();
      for (auto& b : bullets) {
          gm->removeSubObject(b);
      }

      auto powerups = gm->getAllObjectOfType<Powerup>();
      for (auto& p : powerups) {
          gm->removeSubObject(p);
      }

      auto space_objects = gm->getAllObjectOfType<SpaceObject>();
      for (auto& s : space_objects) {
          gm->removeSubObject(s);
      }
      displayScore();
  }

  if(_clock.isRunning()){
      std::ostringstream time_;
      auto t = int(45 - _clock.elapsed());
      if(t <= 0 or gm->gameOver) time_ << 0;
      else time_ << t;

      std::string time_str = time_.str();
      if(t < 10)
      {
          printText("REMAINING TIME : " + time_str,
                    glm::vec3(30.0f, -27.0f, -100.0f),
                    glm::vec3(1.0f, 0.0f, 0.0f),
                    1);
      }
      else
      {
          printText("REMAINING TIME : " + time_str,
                    glm::vec3(30.0f, -27.0f, -100.0f),
                    glm::vec3(1.0f, 1.0f, 1.0f),
                    1);
      }
  }

  std::ostringstream currentscore_;
  currentscore_ << gm->getCurrentScore();
  std::string currentscore_str = currentscore_.str();

  printText("CURRENT-SCORE : " + currentscore_str,
                    glm::vec3(30.0f, -30.0f, -100.0f),
                    glm::vec3(1.0f, 1.0f, 1.0f),
                    1);

  std::ostringstream spaceship_life_;
  spaceship_life_ << gm->getSpaceshipLife();
  std::string spaceship_life_str = spaceship_life_.str();

  printText("SPACE-SHIP LIFE : " + spaceship_life_str,
                    glm::vec3(30.0f, -33.0f, -100.0f),
                    glm::vec3(1.0f, 1.0f, 1.0f),
                    1);

  GLenum err = glGetError();
  if (err != GL_NO_ERROR)
  {
      std::cout << "open gl error: " << gluErrorString(err) << std::endl;
                   std::cout.flush();
  }
  glutSwapBuffers();
  glutPostRedisplay();

}

void keyDown(unsigned char key, int x, int y)
{
  switch (key) 
  {
    case 'q':
    case 27:
      glutDestroyWindow(window);
#ifndef _WIN32
      // Must use this with regular glut, since it never returns control to main().
      exit(0);
#endif
      break;
      
    case 'w':
      keyPressed[KEY_ID_W]              = true;
      break;
    case 'a':
      keyPressed[KEY_ID_A]              = true;
      break;
    case 's':
      keyPressed[KEY_ID_S]              = true;
      break;
    case 'd':
      keyPressed[KEY_ID_D]              = true;
      break;
    case ' ':
      keyPressed[KEY_ID_SPACE]          = true;
      break;
    case 'c':
      keyPressed[KEY_ID_C]              = true;
      break;
    case 'x':
      keyPressed[KEY_ID_X]              = true;
      break;
    case 'z':
      keyPressed[KEY_ID_Z]              = true;
      break;
  case 't':
      keyPressed[KEY_ID_UPARROW]        = true;
      break;
  case 'g':
      keyPressed[KEY_ID_DOWNARROW]      = true;
      break;
  case 'f':
      keyPressed[KEY_ID_LEFTARROW]      = true;
      break;
  case 'h':
      keyPressed[KEY_ID_H]              = true;
      break;
  case 'j':
      keyPressed[KEY_ID_J]              = true;
      break;
  case 'u':
      keyPressed[KEY_ID_U]              = true;
      break;
  case 'n':
      keyPressed[KEY_ID_N]              = true;
      break;
  case 'p':
      keyPressed[KEY_ID_P]              = true;
      break;
  case 'r':
      keyPressed[KEY_ID_R]              = true;
      break;
  case '1':
      keyPressed[KEY_ID_1]              = true;
      break;
  case '2':
      keyPressed[KEY_ID_2]              = true;
      break;
  case '3':
      keyPressed[KEY_ID_3]              = true;
      break;
  case '4':
      keyPressed[KEY_ID_4]              = true;
      break;

    default:
      glutPostRedisplay();
  }
}

void keyUp(unsigned char key, int x, int y)
{
  switch (key) 
  {
    case 'w':
      keyPressed[KEY_ID_W]              = false;
      break;
    case 'a':
      keyPressed[KEY_ID_A]              = false;
      break;
    case 's':
      keyPressed[KEY_ID_S]              = false;
      break;
    case 'd':
      keyPressed[KEY_ID_D]              = false;
      break;
    case ' ':
      keyPressed[KEY_ID_SPACE]          = false;
      break;
    case 'c':
      keyPressed[KEY_ID_C]              = false;
      break;
    case 'x':
      keyPressed[KEY_ID_X]              = false;
      break;
    case 'z':
      keyPressed[KEY_ID_Z]              = false;
      break;
  case 't':
      keyPressed[KEY_ID_UPARROW]        = false;
      break;
  case 'g':
      keyPressed[KEY_ID_DOWNARROW]      = false;
      break;
  case 'f':
      keyPressed[KEY_ID_LEFTARROW]      = false;
      break;
  case 'h':
      keyPressed[KEY_ID_H]              = false;
      break;
  case 'j':
      keyPressed[KEY_ID_J]              = false;
      break;
  case 'u':
      keyPressed[KEY_ID_U]              = false;
      break;
  case 'n':
      keyPressed[KEY_ID_N]              = false;
      break;
  case 'p':
      keyPressed[KEY_ID_P]              = false;
      break;
  case 'r':
      keyPressed[KEY_ID_R]              = false;
  }
}

void mousePressed(int button, int state, int posX, int posY)
{
  if(button==GLUT_LEFT_BUTTON && state==GLUT_DOWN)
  {
    mousePosX = posX;
    mousePosY = posY;
    keyPressed[MOUSE_LEFT_BUTTON_DOWN] = true;
  }  
  if(button==GLUT_LEFT_BUTTON && state==GLUT_UP)
    keyPressed[MOUSE_LEFT_BUTTON_DOWN] = false;
}

void mouseMoved(int posX, int posY)
{
  if(keyPressed[MOUSE_LEFT_BUTTON_DOWN])
  {
    int diffX = posX - mousePosX;
    mousePosX = posX;
    int diffY = posY - mousePosY;
    mousePosY = posY;
    
    // Implement quaternion based mouse move
    gm->getCam()->mouseQuaternion(diffX, diffY);
  }
}

void reshape(int w, int h)
{
  glViewport(0, 0, (GLsizei) w, (GLsizei) h); 
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
//  glOrtho(-50, 700, -50, 700, -50, 50);
  gluPerspective(60.0f, float(w)/float(h) ,1.0f, 3000.0f);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
//  gluLookAt(0.0, 0.0, 10.0,     0.0, 0.0, 0.0,    0.0, 1.0, 0.0);
}
int main(int argc, char** argv)
{

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB);
  glutInitWindowSize(700, 700); 
  glutInitWindowPosition(10, 10);
  window = glutCreateWindow("Space Invaders 3D");
  init();
  glutKeyboardFunc(keyDown);
  glutKeyboardUpFunc(keyUp);
  glutReshapeFunc(reshape);
  glutDisplayFunc(display);
  glutMouseFunc(mousePressed);
  glutMotionFunc(mouseMoved);

  // Add other callback functions here..

  glutMainLoop();
  return 0;
}


